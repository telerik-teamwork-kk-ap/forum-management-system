﻿namespace Forum_Management_System.Models.QueryParameters
{
    public class CommentQueryParameters
    {
        public string FromUser { get; init; }

        // likes, recent
        public string SortBy { get; init; }

        // true/false

        public void Deconstruct(out string sortBy, out string fromUser)
        {
            fromUser = this.FromUser;
            sortBy = this.SortBy ?? "recent";
        }
    }
}
