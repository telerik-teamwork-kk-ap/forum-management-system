﻿namespace Forum_Management_System.Models.QueryParameters
{
    public class UserQueryParameters
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }

        // ? search by phonenumber only for Admins?
        // ? time created - range
        public string SortBy { get; set; }
        public string Order { get; set; }
        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(this.Username) && string.IsNullOrEmpty(this.FirstName) &&
                 string.IsNullOrEmpty(this.LastName) && string.IsNullOrEmpty(this.Email) &&
                 string.IsNullOrEmpty(this.Role) && string.IsNullOrEmpty(this.SortBy) &&
                 string.IsNullOrEmpty(this.Order);
        }
    }
}
