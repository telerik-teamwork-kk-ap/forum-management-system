﻿namespace Forum_Management_System.Models.QueryParameters
{
    public class PostQueryParameters
    {
        // recentactivity, recent, likes, comments
        public string SortBy { get; init; }

        // today, yesterday, lastweek, lastmonth, months, year, alltime
        public string TimePeriod { get; init; }

        // contains username of user
        public string FromUser { get; init; }

        public string Keyword { get; init; }

        public void Deconstruct(out string sortBy, out string timePeriod, out string fromUser, out string keyword)
        {
            sortBy = this.SortBy ?? "recent";
            timePeriod = this.TimePeriod ?? "alltime";
            fromUser = this.FromUser;
            keyword = this.Keyword ?? "";
        }
    }
}
