﻿using System.ComponentModel.DataAnnotations;

namespace Forum_Management_System.Models
{
    public class Photo : IPhoto
    {
        [Key]
        public int PhotoID { get; set; }
        [Required]
        public string PhotoURL { get; set; }
        [Required]
        public int UserId { get; set; }
        public User User { get; set; }

    }
}
