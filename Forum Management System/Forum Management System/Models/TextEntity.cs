﻿using Forum_Management_System.Models.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Forum_Management_System.Models
{
    public abstract class TextEntity : IText, ILikeable
    {
        protected TextEntity()
        {
            this.TimeCreated = DateTime.Now;
        }

        protected TextEntity(int id, int userID, string body) : this()
        {
            this.ID = id;
            this.UserID = userID;
            this.Body = body;
        }

        [Key]
        public int ID { get; set; }

        [StringLength(8192, MinimumLength = 32, ErrorMessage = ErrorMessages.LengthOutofRange)]
        [Required(AllowEmptyStrings = false)]
        public string Body
        {
            get; set;
        }

        public DateTime TimeCreated { get; set; }


        public int? UserID { get; set; }

        public User User { get; set; }

        // public ICollection<User> LikedBy { get; set; }
        // 
        // public ICollection<User> DislikedBy { get; set; }

        public void Deconstruct(out int id, out User user, out string body, out int likes, out int dislikes, out DateTime timeCreated)
        {
            id = this.ID;
            user = this.User;
            body = this.Body;
            likes = this.Likes;
            dislikes = this.Dislikes;
            timeCreated = this.TimeCreated;
        }

        public int Likes { get; set; } = 0;

        public int Dislikes { get; set; } = 0;
    }
}
