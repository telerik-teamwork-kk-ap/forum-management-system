﻿using Forum_Management_System.Models.Contracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum_Management_System.Models
{
    public class Tag : ITag
    {
        public Tag()
        {

        }

        public Tag(string name)
        {
            this.Name = name;
        }
        public int TagID { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        public ICollection<Post> Posts { get; set; } = new List<Post>();

        public override string ToString()
        {
            return this.Name.ToLower();
        }

        public override bool Equals(object obj)
        {
            var other = obj as Tag;
            return this.Name.ToLower() == other.Name.ToLower();
        }

        public override int GetHashCode()
        {
            return this.Name.ToLower().GetHashCode();
        }
    }
}
