﻿using Forum_Management_System.Models.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Models
{
    public class Comment : TextEntity, IComment
    {
        public Comment() : base()
        {

        }
        public Comment(int id, int userID, string body, int postID) : base(id, userID, body)
        {
            this.IsSolution = false;
            this.PostID = postID;
        }

        public Comment(int id, int userID, string body, int postID, string quote)
            : this(id, userID, body, postID)
        {
            this.Quote = quote;
        }

        public int PostID { get; set; }

        public Post Post { get; }

        public bool IsSolution { get; set; }

        public string Quote { get; set; }

        public void Deconstruct(out int id,
                                out User user,
                                out string body,
                                out int likes,
                                out int dislikes,
                                out DateTime timeCreated,
                                out Post post,
                                out bool isSolution,
                                out string quote)
        {
            base.Deconstruct(out id, out user, out body, out likes, out dislikes, out timeCreated);
            post = this.Post;
            isSolution = this.IsSolution;
            quote = this.Quote;
        }

        public ICollection<CommentVote> Votes { get; set; } = new List<CommentVote>();

    }
}
