﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum_Management_System.Models
{
    public class User : IUser
    {
        public User()
        {

        }
        public User(int userID, string username, string password, string firstName, string lastName, string email)
        {
            this.UserID = userID;
            this.Username = username;
            this.Password = password;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Email = email;
            this.Role = UserRole.user;
            this.TimeCreated = DateTime.Now;
            this.IsBlocked = false;
        }

        [Key]
        [Required(AllowEmptyStrings = false)]
        public int UserID
        {
            get;
            set;
        }

        [Required(AllowEmptyStrings = false)]
        [MinLength(4)]
        [MaxLength(32)]
        public string FirstName
        {
            get;
            set;
        }

        [Required(AllowEmptyStrings = false)]
        [MinLength(4)]
        [MaxLength(32)]
        public string LastName
        {
            get;
            set;
        }

        [Required(AllowEmptyStrings = false)]
        [MinLength(3)]
        [MaxLength(16)]
        public string Username
        {
            get;
            set;
        }

        [Required(AllowEmptyStrings = false)]
        [MinLength(8)]
        [MaxLength(128)]
        public string Password
        {
            get;
            set;
        }

        [Required(AllowEmptyStrings = false)]
        [EmailAddress]
        public string Email
        {
            get;
            set;
        }

        public UserRole Role { get; set; } = UserRole.user;
        public DateTime TimeCreated { get; set; } = DateTime.Now;
        public bool IsBlocked { get; set; } = false;
        public void Deconstruct(out int userID, out string username, out string firstName,
            out string lastName, out string password, out string email, out string role, 
            out string timeCreated, out bool isBlocked)
        {
            userID = this.UserID;
            username = this.Username;
            firstName = this.FirstName;
            lastName = this.LastName;
            password = this.Password;
            email = this.Email;
            role = this.Role.ToString();
            timeCreated = this.TimeCreated.ToShortDateString();
            isBlocked = this.IsBlocked;
        }
        public ICollection<Post> Posts { get; set; } = new List<Post>();
        public ICollection<Comment> Comments { get; set; } = new List<Comment>();
        public Photo Photo { get; set; }
        public ICollection<PostVote> PostVotes { get; set; }
        public ICollection<CommentVote> CommentVotes { get; set; }
        public UserAdditionalInfo UserAdditionalInfo { get; set; }
    }
}
