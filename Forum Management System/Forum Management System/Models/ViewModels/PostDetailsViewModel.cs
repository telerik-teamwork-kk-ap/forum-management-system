﻿using Forum_Management_System.Models.Dtos;
using System.ComponentModel.DataAnnotations;

namespace Forum_Management_System.Models.ViewModels
{
    public class PostDetailsViewModel
    {
        public PostDetailsViewModel(PostResponseDTO postDTO, string commentBody)
        {
            this.PostDTO = postDTO;
            this.CommentBody = commentBody;
        }

        public PostDetailsViewModel()
        {

        }

        public PostResponseDTO PostDTO { get; set; }

        [StringLength(8192, MinimumLength = 32, ErrorMessage = ErrorMessages.LengthOutofRange)]
        [Required(AllowEmptyStrings = false)]
        public string CommentBody { get; set; }

        public int PostID { get; set; }
    }
}
