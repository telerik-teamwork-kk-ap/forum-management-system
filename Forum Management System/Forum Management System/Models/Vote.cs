﻿namespace Forum_Management_System.Models
{
    public class PostVote
    {
        public PostVote()
        {

        }

        public PostVote(int postID, int userID, bool action)
        {
            this.PostID = postID;
            this.UserID = userID;
            this.Action = action;
        }

        public int ID { get; set; }

        public int PostID { get; set; }

        public Post Post { get; set; }

        public int UserID { get; set; }

        public User User { get; set; }

        /// <summary>
        /// For a like it will be true, for a dislike it will be false
        /// </summary>
        public bool Action { get; set; }
    }

    public class CommentVote
    {
        public CommentVote()
        {

        }

        public CommentVote(int commentID, int userID, bool action)
        {
            this.CommentID = commentID;
            this.UserID = userID;
            this.Action = action;
        }

        public int ID { get; set; }

        public int CommentID { get; set; }

        public Comment Comment { get; set; }

        public int UserID { get; set; }

        public User User { get; set; }

        /// <summary>
        /// For a like it will be true, for a dislike it will be false
        /// </summary>
        public bool Action { get; set; }
    }
}
