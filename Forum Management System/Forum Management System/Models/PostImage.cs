﻿namespace Forum_Management_System.Models
{
    public class PostImage
    {
        public PostImage()
        {
        }

        public PostImage(string url)
        {
            this.URL = url;
        }

        public int ID { get; set; }

        public Post Post { get; set; }

        public int PostID { get; set; }

        public string URL { get; set; }
    }
}
