﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Models.Contracts
{
    public interface IText
    {
        int ID { get; set; }
        User User { get; }
        int? UserID { get; }
        string Body { get; set; }
        DateTime TimeCreated { get; }
    }
}
