﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Models.Contracts
{
    public interface IComment : IText, ILikeable
    {
        string Quote { get; }

        bool IsSolution { get; set; }

        Post Post { get; }
    }
}
