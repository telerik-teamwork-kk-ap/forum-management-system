﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Models
{
    public interface IPhoto
    {
        int PhotoID { get; set; }
        string PhotoURL { get; set; }
        public int UserId { get; set; }
    }
}
