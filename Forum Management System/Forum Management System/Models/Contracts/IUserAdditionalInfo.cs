﻿namespace Forum_Management_System.Models
{
    public interface IUserAdditionalInfo
    {
       int UserID { get; }

       string PhoneNumber { get; set; }
    }
}