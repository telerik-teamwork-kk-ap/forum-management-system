﻿using Forum_Management_System.Dtos.Requests;
using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Dtos.Responses;

namespace Forum_Management_System.Models
{
    public interface IUserMapper
    {
        User ConvertToUserCreate(CreateUserDto dto);
        User ConvertToUserUpdate(User user, UpdateUserDto dto);
        UserFullInfoDto ConvertUserToFullInfoDto(User user);
        UserLimitedAccessInfoDto ConvertUserToLimitedInfoDto(User user);
        bool IsPhoneNumberValid(string phoneNumber);
        bool IsEmailValid(string email);
        bool IsPasswordValid(string password);
    }
}