﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Models.Contracts
{
    public interface ITag
    {
        int TagID { get; }
        string Name { get; }
    }
}
