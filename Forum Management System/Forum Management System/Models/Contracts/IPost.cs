﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Models.Contracts
{
    public interface IPost : IText, ILikeable
    {
        ICollection<Comment> Comments { get; }

        // IReadOnlyList<ITag> Tags { get; }

        string Title { get; set; }
    }
}
