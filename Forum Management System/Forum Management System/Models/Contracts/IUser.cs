﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Models
{
    public interface IUser
    {
        int UserID { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Username { get; }
        string Password { get; set; }
        string Email { get; set; }
        UserRole Role { get; set; }
        DateTime TimeCreated { get; }
        public Photo Photo { get; set; }
        bool IsBlocked { get; set; }
        public void Deconstruct(out int userID, out string username, out string firstName,
            out string lastName, out string password, out string email, out string role, 
            out string timeCreated, out bool isBlocked);
    }
}
