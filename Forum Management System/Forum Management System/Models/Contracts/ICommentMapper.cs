﻿using Forum_Management_System.Models.Dtos;

namespace Forum_Management_System.Models.Contracts
{
    public interface ICommentMapper
    {
        Comment ConvertToModel(CommentCreateDTO commentDto);

        Comment UpdateModel(Comment toUpdate, CommentUpdateDTO dto);

        CommentResponseDTO ConvertToCommentResponse(Comment c);
    }
}
