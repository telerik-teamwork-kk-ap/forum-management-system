﻿using Forum_Management_System.Models.Dtos;

namespace Forum_Management_System.Models.Contracts
{
    public interface IPostMapper
    {
        Post ConvertToModel(PostCreateDTO dto);

        Post UpdateModel(Post toUpdate, PostUpdateDTO dto);

        PostResponseInListDTO ConvertToPostResponseInList(Post post);

        PostResponseDTO ConvertToPostResponse(Post post);
    }
}
