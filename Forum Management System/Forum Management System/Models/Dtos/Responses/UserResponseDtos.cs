﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Dtos.Responses
{
        public record UserFullInfoDto(int Id, string Username, string FirstName, 
            string LastName, string Password, string Email, string PhoneNumber, string TimeCreated, 
            string Role, bool IsBlocked, string PhotoURL) : IGetUserDto;
        public record UserLimitedAccessInfoDto(string Username, string FirstName, string 
            LastName, string Role, string TimeCreated, string PhotoURL) : IGetUserDto;
}
