﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Dtos.Responses
{
    public interface IGetUserDto
    {
        string Username { get; }
        string FirstName { get; }
        string LastName { get; }
        string Role { get; }
        string TimeCreated { get; }
        string PhotoURL { get; }
    }
}
