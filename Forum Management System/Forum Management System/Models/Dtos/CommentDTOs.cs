﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum_Management_System.Models.Dtos
{

    public record CommentCreateDTO(
        int UserID,
        int PostID,
        [StringLength(8192, MinimumLength = 32, ErrorMessage = ErrorMessages.LengthOutofRange)]
        [Required(AllowEmptyStrings = false)]
        string Body
        );

    public record CommentUpdateDTO(
        int PostID,
        int ID,
        [StringLength(8192, MinimumLength = 32, ErrorMessage = ErrorMessages.LengthOutofRange)]
        string Body
        );

    public record CommentVoteDTO(
        int CommentID,
        int UsedID,
        [Required]
        bool Action
        );

    /// <summary>
    /// Used for taking only the data needed when a like or dislike request is made.
    /// </summary>
    public record CommentVoteResponseInternalDTO(
        int PostID,
        int ID,
        int Likes,
        int Dislikes,
        List<int> LikedByUserID,
        List<int> DislikedByUserID
        );

    public record CommentResponseDTO(
        int ID,
        string UserUsername,
        string Quote,
        string Body,
        int Likes,
        int Dislikes,
        DateTime TimeCreated,
        bool IsSolution
        );

}
