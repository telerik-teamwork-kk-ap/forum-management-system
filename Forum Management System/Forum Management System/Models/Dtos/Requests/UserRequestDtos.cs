﻿using Forum_Management_System.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Dtos.Requests
{
    public record CreateUserDto(
        [StringLength(32, MinimumLength = 4, ErrorMessage = ErrorMessages.LengthOutofRange),
        Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.RequeredField)]
        string FirstName,

        [StringLength(32, MinimumLength = 4, ErrorMessage = ErrorMessages.LengthOutofRange),
        Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.RequeredField)]
        string LastName,

        [StringLength(16, MinimumLength = 3, ErrorMessage = ErrorMessages.LengthOutofRange),
        Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.RequeredField)]
        string Username,

        [StringLength(128, MinimumLength = 8, ErrorMessage = ErrorMessages.LengthOutofRange),
        Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.RequeredField)]
        string Password,
        [EmailAddress,
            Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.RequeredField)]
        string Email,
        [Url]
        string PhotoURL
        );

    public record CreateAdminDto
    {
        [Range(1, int.MaxValue, ErrorMessage = ErrorMessages.NumberhOutofRange)]
        public int UserID { get; init; }
        public string PhoneNumber { get; set; }
    }

    public class UpdateUserDto
    {
        public UpdateUserDto()
        {

        }
        public UpdateUserDto(string firstname, string lastname, string password, string email, string photourl)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Password = password;
            this.Email = email;
            this.PhotoURL = photourl;
        }
        [StringLength(32, MinimumLength = 4, ErrorMessage = ErrorMessages.LengthOutofRange)]
        public string FirstName { get; set; }
        [StringLength(32, MinimumLength = 4, ErrorMessage = ErrorMessages.LengthOutofRange)]
        public string LastName { get; set; }
        [StringLength(128, MinimumLength = 8, ErrorMessage = ErrorMessages.LengthOutofRange)]
        public string Password { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string PhotoURL { get; set; }
    }

    public record UpdateUserAdditionalFeaturesDto(
    [Phone]
    string PhoneNumber
    );

    public record UpdateRoleDto(
    [Range (0, 1, ErrorMessage = ErrorMessages.NumberhOutofRange)]
    int Role
     );

    public record LoginUserDto(
    [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.RequeredField)]
    string Username,
    [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.RequeredField)]
    string Password
     );

}
