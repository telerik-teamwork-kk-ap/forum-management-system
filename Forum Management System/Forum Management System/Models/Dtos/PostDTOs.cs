﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum_Management_System.Models.Dtos
{
    public record PostCreateDTO(
        int UserID,
        [StringLength(64, MinimumLength = 16, ErrorMessage = ErrorMessages.LengthOutofRange)]
        [Required(AllowEmptyStrings = false)]
        string Title,
        [StringLength(8192, MinimumLength = 32, ErrorMessage = ErrorMessages.LengthOutofRange)]
        [Required(AllowEmptyStrings = false)]
        string Body,
        IEnumerable<string> Tags,
        IEnumerable<string> ImageURLs
        );

    public record PostUpdateDTO(
        int ID,
        [StringLength(64, MinimumLength = 16, ErrorMessage = ErrorMessages.LengthOutofRange)]
        string Title,
        [StringLength(8192, MinimumLength = 32, ErrorMessage = ErrorMessages.LengthOutofRange)]
        string Body,
        IEnumerable<string> Tags
        );

    /// <summary>
    /// Taken from body of the request
    /// </summary>
    /// <param name="ID">ID of the post being voted</param>
    /// <param name="Action">Action is true for a like and false for a dislike</param>
    public record PostVoteDTO(
        int PostID,
        int UserID,
        [Required]
        bool Action
        );

    

    public record PostResponseDTO(
        int ID,
        string UserUsername,
        string Title,
        string Body,
        IEnumerable<string> Tags,
        int Likes,
        int Dislikes,
        IEnumerable<string> ImageURLs,
        IEnumerable<CommentResponseDTO> Comments,
        DateTime TimeCreated
        );

    public record PostResponseInListDTO(
        int ID,
        string UserUsername,
        string Title,
        IEnumerable<string> Tags,
        int Likes,
        int Dislikes,
        DateTime TimeCreated,
        DateTime MostRecentComment,
        int CommentCount
        );
}
