﻿using System.ComponentModel.DataAnnotations;

namespace Forum_Management_System.Models
{
    public class UserAdditionalInfo : IUserAdditionalInfo
    {
        [Key]
        public int InfoID { get; set; }
        [Required]
        public int UserID { get; set; }
        public User User { get; set; }
        public string PhoneNumber { get; set; }
    }
}
