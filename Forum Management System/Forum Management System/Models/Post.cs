﻿using Forum_Management_System.Models.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Models
{
    public class Post : TextEntity, IPost
    {
        public Post() : base()
        {

        }

        public Post(int id, int userID, string body, string title, List<Tag> tags, List<PostImage> images) : base(id, userID, body)
        {
            this.Title = title;
            this.Images = images;
            this.Tags = tags;
        }

        [StringLength(64, MinimumLength = 16, ErrorMessage = ErrorMessages.LengthOutofRange)]
        [Required(AllowEmptyStrings = false)]
        public string Title
        {
            get;
            set;
        }

        public ICollection<Comment> Comments { get; set; } = new List<Comment>();

        public ICollection<PostImage> Images { get; set; } = new List<PostImage>();

        public ICollection<Tag> Tags { get; set; } = new List<Tag>();

        public ICollection<PostVote> Votes { get; set; } = new List<PostVote>();

        public void Deconstruct(out int id,
                                out User user,
                                out string title,
                                out string body,
                                out int likes,
                                out int dislikes,
                                out DateTime timeCreated,
                                out ICollection<Comment> comments,
                                out ICollection<PostImage> images)
        {
            this.Deconstruct(out id, out user, out body, out likes, out dislikes, out timeCreated);
            title = this.Title;
            comments = this.Comments;
            images = this.Images;
        }

    }
}
