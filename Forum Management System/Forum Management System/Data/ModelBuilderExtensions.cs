﻿using Forum_Management_System.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Forum_Management_System.Data
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var users = new List<User>();
            var userAdditionalInfo = new List<UserAdditionalInfo>();
            var photos = new List<Photo>();
            var posts = new List<Post>();
            var comments = new List<Comment>();
            var postImages = new List<Comment>();
            var postVotes = new List<PostVote>();
            var commentVotes = new List<CommentVote>();

            // add items: Admin and Users
            users.Add(new User(1, "Admin", "12345678", "ADMIN", "THE OWNER", "admin.adminov@abv.bg"));
            users.Add(new User(2, "s.dzanaharian", "12345678", "Steven", "Dzanaharian", "s.dzanaharian@gmail.com"));
            users.Add(new User(3, "m.magdalena", "12345678", "Marry", "Magdalena", "m.magdalena@gmail.com"));
            users.Add(new User(4, "JONSON", "12345678", "Just", "Jonson", "jj.jonson@gmail.com"));
            users.Add(new User(5, "little.kitten", "12345678", "Nastiya", "Kirkegorova", "nastenka.little.kitten.1000@gmail.com"));
            users.Add(new User(6, "d.adams", "12345678", "David", "Adams", "d.adams@gmail.com"));
            users[0].Role = UserRole.admin;

            // add additional info for the admin
            userAdditionalInfo.Add(new UserAdditionalInfo {InfoID = 1, UserID = 1, PhoneNumber = "0878682028" });

            // add a default photo
            var firstPhoto = new Photo
            {
                PhotoID = 1,
                UserId = 1,
                PhotoURL = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMvmg3R0ZAKRJ-BPOcI2QcpK7f-AfuNztJ8A&usqp=CAU"
            };
            //firstPhoto.User = users[0];
            photos.Add(firstPhoto);

            // add posts and comments

            posts.Add(new Post(1, 1, "Today I cooked something amazing!!! You should try to replicate it no matter what. All you need is four pink tomatos.", "Delicious tomatos soup", new List<Tag>(), new List<PostImage>()));
            posts.Add(new Post(2, 2, "Today I cooked something nice!!! You should try to replicate it no matter what. All you need is a brisket of beef, some spices and butter.", "Amazing beef roast", new List<Tag>(), new List<PostImage>()));
            posts.Add(new Post(3, 4, "Today I cooked something good!!! You should try to replicate it no matter what. All you need is a kilogram of mushrooms", "Delicious mushroom soup", new List<Tag>(), new List<PostImage>()));
            posts.Add(new Post(4, 6, "Today I cooked something delicious!!! You should try to replicate it no matter what. All you need for this recipe is: Burger buns of your choosing, minced beef, salt pepper, bacon, onion, lettuce and tomato. For the sauce you will need some parsley, and egg and some vegetable fat I recommend olive oil.", "Best burger ever", new List<Tag>(), new List<PostImage>()));
            posts.Add(new Post(5, 6, "Today I cooked something delicious!!! You should try to replicate it no matter what. All you need for this recipe is: Burger buns of your choosing, minced beef, salt pepper, bacon, onion, lettuce and tomato. For the sauce you will need some parsley, and egg and some vegetable fat I recommend olive oil.", "Second Best Burger", new List<Tag>(), new List<PostImage>()));
            
            
            comments.Add( new Comment(1, 2, "I hate tomatos. I can understand how can anyone likes a soup of them.", 1));
            comments.Add( new Comment(2, 3, "I actually made it and it turned out surprisingly good.", 1));
            comments.Add( new Comment(3, 6, "OMG I got you recipe and made it one to one, the roast was the juiciest roast I had ever had.", 2));
            comments.Add( new Comment(4, 5, "I tried your recipe but I didn't cook the beef properly and it was dry, be careful guys use a termometer if you need", 2));
            comments.Add( new Comment(5, 5, "After I dried out by beef brisket I decided to go vegan, this mushroom soup is easy and great for people like me.", 3));
            comments.Add( new Comment(6, 4, "I love mushroom soup but I think some beef stock will make it a lot better.", 3));
            comments.Add( new Comment(7, 2, "What fat:meat ratio do you recommend for this recipe.", 4));
            comments.Add( new Comment(8, 6, "I used 20:80 this time and it turned out great you can go to 30:70 if you want but no more.", 4));
            comments.Add( new Comment(9, 3, "I recommend toasing the buns in some butter it makes the burger not fall apart and adds some graet flavour notes.", 4));
            comments.Add( new Comment(10, 4, "I love burgers. This recipe is very good, here is a link for the recipe for the buns I decided to make: https://www.youtube.com/watch?v=GeQGgzeSQjI. I think it is one of the best recipes for burger buns.", 4));

            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Post>().HasData(posts);
            modelBuilder.Entity<PostVote>().HasData(postVotes);
            modelBuilder.Entity<Comment>().HasData(comments);
            modelBuilder.Entity<CommentVote>().HasData(commentVotes);
            modelBuilder.Entity<PostImage>().HasData(postImages);
            modelBuilder.Entity<Photo>().HasData(photos);
            modelBuilder.Entity<UserAdditionalInfo>().HasData(userAdditionalInfo);
        }
    }
}
