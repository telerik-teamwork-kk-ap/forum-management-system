﻿using Forum_Management_System.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum_Management_System.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) :
            base(options)
        {

        }

        //dbSets / SQL tables

        public DbSet<User> Users { get; set; }

        public DbSet<UserAdditionalInfo> UserAdditionalInfo { get; set; }

        public DbSet<Photo> Photos { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<PostVote> PostVotes { get; set; }

        public DbSet<CommentVote> CommentVotes { get; set; }

        public DbSet<Tag> Tags { get; set; }

        // override model configuration

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // seed database
            modelBuilder.Seed();

            // configure relations

            modelBuilder.Entity<Comment>()
                .HasOne(comment => comment.Post)
                .WithMany(post => post.Comments)
                .HasForeignKey(comment => comment.PostID)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder
                .Entity<Comment>()
                .HasOne(comment => comment.User)
                .WithMany(post => post.Comments)
                .HasForeignKey(comment => comment.UserID)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder
                .Entity<Post>()
                .HasOne(post => post.User)
                .WithMany(user => user.Posts)
                .HasForeignKey(post => post.UserID)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder
                .Entity<User>()
                .HasMany(user => user.Posts)
                .WithOne(post => post.User)
                .HasForeignKey(post => post.UserID)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder
                .Entity<User>()
                .HasMany(user => user.Comments)
                .WithOne(comment => comment.User)
                .HasForeignKey(comment => comment.UserID)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder
                .Entity<Tag>()
                .HasMany(tag => tag.Posts)
                .WithMany(post => post.Tags)
                .UsingEntity(j => j.ToTable("PostTags"));

            modelBuilder.Entity<PostVote>()
                .HasOne(vote => vote.Post)
                .WithMany(post => post.Votes)
                .HasForeignKey(vote => vote.PostID)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<CommentVote>()
                .HasOne(vote => vote.Comment)
                .WithMany(post => post.Votes)
                .HasForeignKey(vote => vote.CommentID)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<PostVote>()
                .HasOne(vote => vote.User)
                .WithMany(user => user.PostVotes)
                .HasForeignKey(vote => vote.UserID)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<CommentVote>()
                .HasOne(vote => vote.User)
                .WithMany(user => user.CommentVotes)
                .HasForeignKey(vote => vote.UserID)
                .OnDelete(DeleteBehavior.NoAction);
        }


    }
}
