﻿namespace Forum_Management_System
{
    public static class ErrorMessages
    {
        public const string LengthOutofRange = "String length for {0} must be between {2} and {1}.";
        public const string NumberhOutofRange = "{0} must be between {1} and {2}.";
        public const string RequeredField = "The field '{0}' is required.";
        public const string EntityNotFound = "{0} with {1} {2} was not found.";
        public const string DuplicateItem = "A {0} with {1} {2} already exists.";
        public const string NotAuthorized = "You are not authorized for this action.";
        public const string EmptyCollection = "This collection is empty.";
        public const string WrongCriteria = "Unexisting value.";
        public const string InvalidParameter = "Invalid credentials: username or password.";
        public const string LoggedUser = "You are already logged in.";
        public const string BlockedUser = "You are currently blocked. You can't create posts and comments.";
        public const string SameRole = "User with id {0} is already at role {1}.";
    }
}

