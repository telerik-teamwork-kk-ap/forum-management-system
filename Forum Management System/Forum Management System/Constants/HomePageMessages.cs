﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Constants
{
    public static class HomePageMessages
    {
        public const string UserCount = "Currently {0} people are using our platform to share their cooking knowledge, draw ideas for the next meal, " +
            "admire the art of well prepared and delicious food and chat with soulmates chefs. Wanna join our cooking society? Please check the register button below! ;)";
        public const string PostCount = "Browse for cooking adventures among {0} posts in our webforum.";
    }
}
