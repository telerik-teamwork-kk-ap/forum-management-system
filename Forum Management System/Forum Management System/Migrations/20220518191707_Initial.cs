﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Forum_Management_System.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    TagID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.TagID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    Username = table.Column<string>(type: "nvarchar(16)", maxLength: 16, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Role = table.Column<int>(type: "int", nullable: false),
                    TimeCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsBlocked = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserID);
                });

            migrationBuilder.CreateTable(
                name: "Photos",
                columns: table => new
                {
                    PhotoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PhotoURL = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Photos", x => x.PhotoID);
                    table.ForeignKey(
                        name: "FK_Photos_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    Body = table.Column<string>(type: "nvarchar(max)", maxLength: 8192, nullable: false),
                    TimeCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserID = table.Column<int>(type: "int", nullable: true),
                    Likes = table.Column<int>(type: "int", nullable: false),
                    Dislikes = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Posts_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "UserAdditionalInfo",
                columns: table => new
                {
                    InfoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<int>(type: "int", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAdditionalInfo", x => x.InfoID);
                    table.ForeignKey(
                        name: "FK_UserAdditionalInfo_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PostID = table.Column<int>(type: "int", nullable: false),
                    IsSolution = table.Column<bool>(type: "bit", nullable: false),
                    Quote = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Body = table.Column<string>(type: "nvarchar(max)", maxLength: 8192, nullable: false),
                    TimeCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserID = table.Column<int>(type: "int", nullable: true),
                    Likes = table.Column<int>(type: "int", nullable: false),
                    Dislikes = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostID",
                        column: x => x.PostID,
                        principalTable: "Posts",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_Comments_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "PostImage",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PostID = table.Column<int>(type: "int", nullable: false),
                    URL = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostImage", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PostImage_Posts_PostID",
                        column: x => x.PostID,
                        principalTable: "Posts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostTags",
                columns: table => new
                {
                    PostsID = table.Column<int>(type: "int", nullable: false),
                    TagsTagID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostTags", x => new { x.PostsID, x.TagsTagID });
                    table.ForeignKey(
                        name: "FK_PostTags_Posts_PostsID",
                        column: x => x.PostsID,
                        principalTable: "Posts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostTags_Tags_TagsTagID",
                        column: x => x.TagsTagID,
                        principalTable: "Tags",
                        principalColumn: "TagID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostVotes",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PostID = table.Column<int>(type: "int", nullable: false),
                    UserID = table.Column<int>(type: "int", nullable: false),
                    Action = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostVotes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PostVotes_Posts_PostID",
                        column: x => x.PostID,
                        principalTable: "Posts",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_PostVotes_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID");
                });

            migrationBuilder.CreateTable(
                name: "CommentVotes",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CommentID = table.Column<int>(type: "int", nullable: false),
                    UserID = table.Column<int>(type: "int", nullable: false),
                    Action = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommentVotes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CommentVotes_Comments_CommentID",
                        column: x => x.CommentID,
                        principalTable: "Comments",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_CommentVotes_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID");
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserID", "Email", "FirstName", "IsBlocked", "LastName", "Password", "Role", "TimeCreated", "Username" },
                values: new object[,]
                {
                    { 1, "admin.adminov@abv.bg", "ADMIN", false, "THE OWNER", "12345678", 1, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(4485), "Admin" },
                    { 2, "s.dzanaharian@gmail.com", "Steven", false, "Dzanaharian", "12345678", 0, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(4872), "s.dzanaharian" },
                    { 3, "m.magdalena@gmail.com", "Marry", false, "Magdalena", "12345678", 0, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(4878), "m.magdalena" },
                    { 4, "jj.jonson@gmail.com", "Just", false, "Jonson", "12345678", 0, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(4883), "JONSON" },
                    { 5, "nastenka.little.kitten.1000@gmail.com", "Nastiya", false, "Kirkegorova", "12345678", 0, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(4952), "little.kitten" },
                    { 6, "d.adams@gmail.com", "David", false, "Adams", "12345678", 0, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(4960), "d.adams" }
                });

            migrationBuilder.InsertData(
                table: "Photos",
                columns: new[] { "PhotoID", "PhotoURL", "UserId" },
                values: new object[] { 1, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMvmg3R0ZAKRJ-BPOcI2QcpK7f-AfuNztJ8A&usqp=CAU", 1 });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "ID", "Body", "Dislikes", "Likes", "TimeCreated", "Title", "UserID" },
                values: new object[,]
                {
                    { 1, "Today I cooked something amazing!!! You should try to replicate it no matter what. All you need is four pink tomatos.", 0, 0, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(8076), "Delicious tomatos soup", 1 },
                    { 2, "Today I cooked something nice!!! You should try to replicate it no matter what. All you need is a brisket of beef, some spices and butter.", 0, 0, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(9232), "Amazing beef roast", 2 },
                    { 3, "Today I cooked something good!!! You should try to replicate it no matter what. All you need is a kilogram of mushrooms", 0, 0, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(9245), "Delicious mushroom soup", 4 },
                    { 4, "Today I cooked something delicious!!! You should try to replicate it no matter what. All you need for this recipe is: Burger buns of your choosing, minced beef, salt pepper, bacon, onion, lettuce and tomato. For the sauce you will need some parsley, and egg and some vegetable fat I recommend olive oil.", 0, 0, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(9249), "Best burger ever", 6 },
                    { 5, "Today I cooked something delicious!!! You should try to replicate it no matter what. All you need for this recipe is: Burger buns of your choosing, minced beef, salt pepper, bacon, onion, lettuce and tomato. For the sauce you will need some parsley, and egg and some vegetable fat I recommend olive oil.", 0, 0, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(9253), "Second Best Burger", 6 }
                });

            migrationBuilder.InsertData(
                table: "UserAdditionalInfo",
                columns: new[] { "InfoID", "PhoneNumber", "UserID" },
                values: new object[] { 1, "0878682028", 1 });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "ID", "Body", "Dislikes", "IsSolution", "Likes", "PostID", "Quote", "TimeCreated", "UserID" },
                values: new object[,]
                {
                    { 1, "I hate tomatos. I can understand how can anyone likes a soup of them.", 0, false, 0, 1, null, new DateTime(2022, 5, 18, 22, 17, 6, 800, DateTimeKind.Local).AddTicks(9718), 2 },
                    { 2, "I actually made it and it turned out surprisingly good.", 0, false, 0, 1, null, new DateTime(2022, 5, 18, 22, 17, 6, 801, DateTimeKind.Local).AddTicks(63), 3 },
                    { 3, "OMG I got you recipe and made it one to one, the roast was the juiciest roast I had ever had.", 0, false, 0, 2, null, new DateTime(2022, 5, 18, 22, 17, 6, 801, DateTimeKind.Local).AddTicks(71), 6 },
                    { 4, "I tried your recipe but I didn't cook the beef properly and it was dry, be careful guys use a termometer if you need", 0, false, 0, 2, null, new DateTime(2022, 5, 18, 22, 17, 6, 801, DateTimeKind.Local).AddTicks(74), 5 },
                    { 5, "After I dried out by beef brisket I decided to go vegan, this mushroom soup is easy and great for people like me.", 0, false, 0, 3, null, new DateTime(2022, 5, 18, 22, 17, 6, 801, DateTimeKind.Local).AddTicks(77), 5 },
                    { 6, "I love mushroom soup but I think some beef stock will make it a lot better.", 0, false, 0, 3, null, new DateTime(2022, 5, 18, 22, 17, 6, 801, DateTimeKind.Local).AddTicks(82), 4 },
                    { 7, "What fat:meat ratio do you recommend for this recipe.", 0, false, 0, 4, null, new DateTime(2022, 5, 18, 22, 17, 6, 801, DateTimeKind.Local).AddTicks(85), 2 },
                    { 8, "I used 20:80 this time and it turned out great you can go to 30:70 if you want but no more.", 0, false, 0, 4, null, new DateTime(2022, 5, 18, 22, 17, 6, 801, DateTimeKind.Local).AddTicks(88), 6 },
                    { 9, "I recommend toasing the buns in some butter it makes the burger not fall apart and adds some graet flavour notes.", 0, false, 0, 4, null, new DateTime(2022, 5, 18, 22, 17, 6, 801, DateTimeKind.Local).AddTicks(91), 3 },
                    { 10, "I love burgers. This recipe is very good, here is a link for the recipe for the buns I decided to make: https://www.youtube.com/watch?v=GeQGgzeSQjI. I think it is one of the best recipes for burger buns.", 0, false, 0, 4, null, new DateTime(2022, 5, 18, 22, 17, 6, 801, DateTimeKind.Local).AddTicks(95), 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostID",
                table: "Comments",
                column: "PostID");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserID",
                table: "Comments",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_CommentVotes_CommentID",
                table: "CommentVotes",
                column: "CommentID");

            migrationBuilder.CreateIndex(
                name: "IX_CommentVotes_UserID",
                table: "CommentVotes",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_UserId",
                table: "Photos",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PostImage_PostID",
                table: "PostImage",
                column: "PostID");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_UserID",
                table: "Posts",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_PostTags_TagsTagID",
                table: "PostTags",
                column: "TagsTagID");

            migrationBuilder.CreateIndex(
                name: "IX_PostVotes_PostID",
                table: "PostVotes",
                column: "PostID");

            migrationBuilder.CreateIndex(
                name: "IX_PostVotes_UserID",
                table: "PostVotes",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_UserAdditionalInfo_UserID",
                table: "UserAdditionalInfo",
                column: "UserID",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommentVotes");

            migrationBuilder.DropTable(
                name: "Photos");

            migrationBuilder.DropTable(
                name: "PostImage");

            migrationBuilder.DropTable(
                name: "PostTags");

            migrationBuilder.DropTable(
                name: "PostVotes");

            migrationBuilder.DropTable(
                name: "UserAdditionalInfo");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
