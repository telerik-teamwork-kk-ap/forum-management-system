﻿using Forum_Management_System.Models;
using Forum_Management_System.Models.QueryParameters;
using System.Linq;

namespace Forum_Management_System.Repositories
{
    public interface ICommentsRepository
    {
		Comment Get(int postID, int id);
		IQueryable<Comment> Get(int postID, CommentQueryParameters filterParameters = null);
		Comment Create(Comment comment);
		Comment Update(Comment comment);
		Comment Delete(int postID, int id);
		Comment Vote(int id, CommentVote vote);
	}
}
