﻿using Forum_Management_System.Models;
using Forum_Management_System.Models.Contracts;
using Forum_Management_System.Models.QueryParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Repositories
{
    public interface IUsersRepository
    {
        IQueryable<User> Get();
        User Get(int id);
        User Get(string name);
        IQueryable<User> Get(UserQueryParameters filterParameters);
        User CreateUser(User user);
        User CreateAdmin(User user, string phoneNumber);
        User Update(User usertoUpdate);
        User UpdateAdminAdditionalInfo(UserAdditionalInfo info);
        User Delete(User userToDelete);
        void DeleteAdminAdditionalInfo(UserAdditionalInfo info);
        bool IsEmailUnique(string email);
        int UserCount();
    }
}
