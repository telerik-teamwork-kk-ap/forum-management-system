﻿using Forum_Management_System.Models;
using Forum_Management_System.Models.Contracts;
using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Models.QueryParameters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum_Management_System.Repositories
{
    public interface IPostsRepository
    {
        Post Get(int id);

        IQueryable<Post> Get(PostQueryParameters filterParameters = null);

        Post Create(Post post);

        Post Update(Post post);

        Post Delete(int id);

        int PostsCount();

        Dictionary<string, int> MostCommentedPostsForAPI();

        Dictionary<string, DateTime> MostRecentPostsForAPI();

        Post Vote(int id, PostVote vote);

        IQueryable<Post> MostRecentPostsForMVC();
        IQueryable<Post> MostCommentedPostsForMVC();

    }
}
