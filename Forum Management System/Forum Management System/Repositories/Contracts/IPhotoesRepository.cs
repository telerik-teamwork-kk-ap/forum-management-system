﻿using Forum_Management_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace Forum_Management_System.Repositories
{
    public interface IPhotosRepository

    {
        IQueryable<Photo> Get();
        Photo Get(int photoID);
        Photo GetByUserID(int userID);
        Photo Create(User user, string photoURL);
        Photo Update(User user, string photoURL);
        Photo Delete(int userID);
    }
}