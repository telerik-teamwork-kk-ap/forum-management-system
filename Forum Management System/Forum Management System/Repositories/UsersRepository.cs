﻿using Forum_Management_System.Data;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Models;
using Forum_Management_System.Models.QueryParameters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum_Management_System.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly ApplicationContext context;
        public UsersRepository(ApplicationContext context)
        {
            this.context = context;
        }
        public User CreateUser(User user)
        {
            var createdUser = this.context.Users.Add(user);
            this.context.SaveChanges();
            return createdUser.Entity;
        }

        public User CreateAdmin(User user, string phoneNumber)
        {
            user.Role = UserRole.admin;

            if (!string.IsNullOrEmpty(phoneNumber))
            {
                this.context.UserAdditionalInfo.Add(new UserAdditionalInfo { UserID = user.UserID, PhoneNumber = phoneNumber });
            }

            this.context.SaveChanges();
            return user;
        }

        public User Delete(User userToDelete)
        {
            // Ensure to delete user's additional info if any exists.

            var infoToRemove = this.context.UserAdditionalInfo.FirstOrDefault(u => u.UserID == userToDelete.UserID);

            if (infoToRemove is not null)
            {
                this.context.UserAdditionalInfo.Remove(infoToRemove);
            }

            // Delete user from database.

            this.context.Users.Remove(userToDelete);
            this.context.SaveChanges();
            return userToDelete;
        }

        public IQueryable<User> Get()
        {
            var result = this.context.Users.Include(user => user.UserAdditionalInfo)
                                           .Include(user => user.Photo);
            return result;
        }

        public User Get(int id)
        {
            var user = this.context.Users.Where(user => user.UserID == id)
                                    .Include(user => user.UserAdditionalInfo)
                                    .Include(user => user.Photo)
                                    .FirstOrDefault();
            return user ?? throw new EntityNotFoundException(String.Format(ErrorMessages.EntityNotFound, "User", "id", id));
        }

        public User Get(string username)
        {
            var usersWithName = this.context.Users.Where(user => user.Username == username)
                                             .Include(user => user.UserAdditionalInfo)
                                             .Include(user => user.Photo)
                                             .FirstOrDefault();
            return usersWithName ?? throw new EntityNotFoundException(String.Format(ErrorMessages.EntityNotFound, "User", "username", username));
        }

        public User Update(User userToUpdate)
        {
            var updatedUser = this.context.Users.Update(userToUpdate);

            this.context.SaveChanges();

            return updatedUser.Entity;
        }

        public User UpdateAdminAdditionalInfo(UserAdditionalInfo info)
        {
            var updatedInfo = this.context.UserAdditionalInfo.Update(info);

            this.context.SaveChanges();

            var updatedUser = this.context.Users.First(u => u.UserID == updatedInfo.Entity.UserID);

            return updatedUser;
        }

        public void DeleteAdminAdditionalInfo(UserAdditionalInfo info)
        {
            this.context.UserAdditionalInfo.Remove(info);
            this.context.SaveChanges();
        }

        public IQueryable<User> Get(UserQueryParameters filterParameters)
        {
            string username = !string.IsNullOrEmpty(filterParameters.Username) ? filterParameters.Username.ToLowerInvariant() : string.Empty;
            string firstName = !string.IsNullOrEmpty(filterParameters.FirstName) ? filterParameters.FirstName.ToLowerInvariant() : string.Empty;
            string lastName = !string.IsNullOrEmpty(filterParameters.LastName) ? filterParameters.LastName.ToLowerInvariant() : string.Empty;
            string email = !string.IsNullOrEmpty(filterParameters.Email) ? filterParameters.Email.ToLowerInvariant() : string.Empty;
            string role = !string.IsNullOrEmpty(filterParameters.Role) ? filterParameters.Role.ToLowerInvariant() : string.Empty;
            string sortCriteria = !string.IsNullOrEmpty(filterParameters.SortBy) ? filterParameters.SortBy.ToLowerInvariant() : string.Empty;
            string sortOrder = !string.IsNullOrEmpty(filterParameters.Order) ? filterParameters.Order.ToLowerInvariant() : string.Empty;

            IQueryable<User> result = this.context.Users.Include(user => user.UserAdditionalInfo)
                                           .Include(user => user.Photo);

            result = this.FilterByUsername(result, username);
            result = this.FilterByFirstName(result, firstName);
            result = this.FilterByLastName(result, lastName);
            result = this.FilterByEmail(result, email);
            result = this.FilterByRole(result, role);
            result = this.SortBy(result, sortCriteria);
            result = this.Order(result, sortOrder);
            return result;
        }
        private IQueryable<User> Order(IQueryable<User> result, string sortOrder)
        {
            return (sortOrder == "desc") ? result.Reverse() : result;
        }
        private IQueryable<User> SortBy(IQueryable<User> result, string sortCriteria)
        {
            switch (sortCriteria)
            {
                case "username":
                    return result.OrderBy(user => user.Username);
                case "firstName":
                    return result.OrderBy(user => user.FirstName);
                case "lastName":
                    return result.OrderBy(user => user.LastName);
                case "email":
                    return result.OrderBy(user => user.Email);
                case "id":
                    return result.OrderBy(user => user.UserID);
                default:
                    return result;
            }
        }
        private IQueryable<User> FilterByRole(IQueryable<User> result, string role)
        {

            if (!string.IsNullOrEmpty(role))
            {
                if (Enum.TryParse(role, true, out UserRole userRole) &&
                    (int)userRole < Enum.GetValues(typeof(UserRole)).Length)
                {
                    return result.Where(u => u.Role == userRole);
                }
                else
                {
                    throw new EntityNotFoundException(ErrorMessages.WrongCriteria);
                }
            }
            else
            {
                return result;
            }

        }
        private IQueryable<User> FilterByEmail(IQueryable<User> result, string email)
        {
            return result.Where(u => u.Email.Contains(email));
        }
        private IQueryable<User> FilterByLastName(IQueryable<User> result, string lastName)
        {
            return result.Where(u => u.LastName.Contains(lastName));
        }
        private IQueryable<User> FilterByFirstName(IQueryable<User> result, string firstName)
        {
            return result.Where(u => u.FirstName.Contains(firstName));
        }
        private IQueryable<User> FilterByUsername(IQueryable<User> result, string username)
        {
            return result.Where(u => u.Username.Contains(username));
        }

        public bool IsEmailUnique(string email)
        {
            return this.context.Users.FirstOrDefault(u => u.Email == email) == null;
        }
        public int UserCount()
        {
            return this.context.Users.Count();
        }
    }
}
