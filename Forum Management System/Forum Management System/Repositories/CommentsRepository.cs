﻿using Forum_Management_System.Data;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Models;
using Forum_Management_System.Models.QueryParameters;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Forum_Management_System.Repositories
{
    public class CommentsRepository : ICommentsRepository
    {
        private readonly ApplicationContext context;

        public CommentsRepository(ApplicationContext context)
        {

            this.context = context;
        }

        public Comment Create(Comment comment)
        {
            this.context.Comments.Add(comment);
            this.context.SaveChanges();
            return comment;
        }

        public Comment Delete(int postID, int id)
        {
            Comment toDelete = this.Get(postID, id);
            this.context.Comments.Remove(toDelete);
            this.context.SaveChanges();
            return toDelete;
        }

        public Comment Get(int postID, int id)
        {
            var comment = this.context.Comments.Where(c => c.Post.ID == postID)
                                               .Where(c => c.ID == id)
                                               .Include(c => c.User)
                                               .FirstOrDefault();

            string message = string.Format(ErrorMessages.EntityNotFound, "Comment", "ID", id);

            return comment ?? throw new EntityNotFoundException(message);
        }

        public IQueryable<Comment> Get(int postID = 0, CommentQueryParameters filterParameters = null)
        {
            var (sortBy, fromUser) = filterParameters;
            IQueryable<Comment> result = this.context.Comments.Where(c => postID == 0 || c.PostID == postID)
                                                              .OrderBy(c => c.IsSolution);
            if (filterParameters is null)
            {
                return result;
            }

            if (fromUser is not null)
            {
                result = result.Where(c => c.User.Username == fromUser);
            }

            switch (sortBy.ToLowerInvariant())
            {
                /*
                case "likes":
                    result = result.OrderBy(c => c.Likes);
                    break;
                */
                case "recent":
                    result = result.OrderBy(c => c.TimeCreated);
                    break;
                case "unordered":
                    break;
                default:
                    string message = string.Format(ErrorMessages.InvalidParameter, "sortBy");
                    throw new InvalidUserInputException(message);
            }
            

            return result;
        }

        public Comment Update(Comment comment)
        {
            this.context.Update(comment);
            this.context.SaveChanges();
            return comment;
        }

        public Comment Vote(int id, CommentVote vote)
        {
            string message = string.Format(ErrorMessages.EntityNotFound, "Comment", "ID", id);
            var comment = this.context.Comments.Where(c => c.ID == id)
                                         .Include(c => c.Votes)
                                         .FirstOrDefault() ?? throw new EntityNotFoundException(message);

            var oldVote = this.context.CommentVotes.Where(v => v.CommentID == vote.CommentID && v.UserID == vote.UserID)
                                                .FirstOrDefault();

            bool found = oldVote is not null;

            if (found)
            {
                this.RemoveVote(comment, oldVote);
                if (!oldVote.Action ^ vote.Action)
                {
                    this.context.Comments.Update(comment);
                    this.context.SaveChanges();
                    return comment;
                }
            }

            return this.AddVote(vote, comment);
        }

        /// <summary>
        /// This is a helper method and it doesn't save changes to the context like AddVote method
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="toRemove"></param>
        private void RemoveVote(Comment comment, CommentVote toRemove)
        {
            if (toRemove.Action)
            {
                comment.Likes--;
            }
            else
            {
                comment.Dislikes--;
            }

            this.context.CommentVotes.Remove(toRemove);
        }

        private Comment AddVote(CommentVote vote, Comment comment)
        {
            if (vote.Action)
            {
                comment.Likes++;
            }
            else
            {
                comment.Dislikes++;
            }

            this.context.CommentVotes.Add(vote);
            this.context.Comments.Update(comment);
            this.context.SaveChanges();
            return comment;
        }
    }
}
