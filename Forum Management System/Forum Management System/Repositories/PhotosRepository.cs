﻿using Forum_Management_System.Data;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum_Management_System.Repositories
{
    public class PhotosRepository : IPhotosRepository
    {
        private readonly ApplicationContext context;
        public PhotosRepository(ApplicationContext context)
        {
            this.context = context;
        }
        public Photo Create(User user, string photoURL)
        {
            var photo = new Photo
            {
                PhotoURL = photoURL,
                UserId = user.UserID,
                User = user
            };

            var createdPhoto = this.context.Add(photo);
            this.context.SaveChanges();
            return createdPhoto.Entity;
        }

        public Photo Delete(int userID)
        {
            var photo = this.GetByUserID(userID);
            var updatedPhoto = this.context.Photos.Remove(photo);
            this.context.SaveChanges();
            return updatedPhoto.Entity;
        }

        public IQueryable<Photo> Get()
        {
            return this.context.Photos;
        }

        public Photo Get(int photoID)
        {
            return this.context.Photos.FirstOrDefault(p => p.PhotoID == photoID) 
                ?? throw new EntityNotFoundException(String.Format(ErrorMessages.EntityNotFound, "Photo", "photoID", photoID));   
        }

        public Photo GetByUserID(int userID)
        {
            return this.context.Photos.FirstOrDefault(p => p.UserId == userID);
        }

        public Photo Update(User user, string imageURL)
        {
            var photoToUpdate = this.GetByUserID(user.UserID);

            if (photoToUpdate is null)
            {
                return Create(user, imageURL);
            }

            photoToUpdate.PhotoURL = imageURL;
            var updatedPhoto = this.context.Update(photoToUpdate);
            this.context.SaveChanges();
            return updatedPhoto.Entity;
        }

    }
}
