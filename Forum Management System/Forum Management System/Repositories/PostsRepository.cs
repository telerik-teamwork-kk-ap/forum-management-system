﻿using Forum_Management_System.Data;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Models;
using Forum_Management_System.Models.QueryParameters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum_Management_System.Repositories
{
    public class PostsRepository : IPostsRepository
    {
        private readonly ApplicationContext context;

        public PostsRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public Post Create(Post post)
        {
            post.Tags = post.Tags.Select(t => this.context.Tags.FirstOrDefault(dbT => dbT.Name == t.Name) ?? t).ToList();
            this.context.Posts.Attach(post);
            this.context.SaveChanges();
            return post;
        }

        public Post Delete(int id)
        {
            Post toRemove = this.Get(id);
            this.context.Posts.Remove(toRemove);
            this.context.SaveChanges();
            return toRemove;
        }

        public Post Get(int id)
        {
            var post = this.context.Posts.Where(p => p.ID == id)
                                         .Include(p => p.User)
                                         .Include(p => p.Tags)
                                         .Include(p => p.Comments)
                                            .ThenInclude(c => c.User)
                                         .FirstOrDefault();
            string error = string.Format(ErrorMessages.EntityNotFound, "Post", "ID", id);

            return post ?? throw new EntityNotFoundException(error);
        }
        public IQueryable<Post> Get(PostQueryParameters filterParameters = null)
        {
            IQueryable<Post> result = this.context.Posts;

            if (filterParameters is null)
            {
                return result;
            }

            var (sortBy, timePeriod, fromUser, keyword) = filterParameters;
            result = this.context.Posts.Where(p => p.Title.Contains(keyword) || p.Body.Contains(keyword));

            if (fromUser is not null)
            {
                result = result.Where(p => p.User != null && p.User.Username == fromUser);
            }

            string message = string.Format(ErrorMessages.InvalidParameter, "timePeriod");

            DateTime minDate = timePeriod.ToLowerInvariant() switch
            {
                "today" => DateTime.UtcNow.AddDays(-1),
                "yesterday" => DateTime.UtcNow.AddDays(-2),
                "lastweek" => DateTime.UtcNow.AddDays(-7),
                "lastmonth" => DateTime.UtcNow.AddMonths(-1),
                "months" => DateTime.UtcNow.AddMonths(-6),
                "year" => DateTime.UtcNow.AddYears(-1),
                "alltime" => DateTime.MinValue,
                _ => throw new InvalidUserInputException(message)

            };

            if (minDate != DateTime.MinValue)
            {
                result = result.Where(p => p.TimeCreated >= minDate);
            }

            switch (sortBy.ToLowerInvariant())
            {
                case "recentactivity":
                    result = result.OrderByDescending(p => p.Comments.Max(c => c.TimeCreated));
                    break;
                case "recent":
                    result = result.OrderByDescending(p => p.TimeCreated);
                    break;
                case "unordered":
                    break;       
                case "likes":
                    result = result.OrderByDescending(p => p.Likes - p.Dislikes);
                    break;
                case "comments":
                    result = result.OrderByDescending(p => p.Comments.Count);
                    break;
            }
            return result;
        }
        public Post Update(Post post)
        {
            post.Tags = post.Tags.Select(t => this.context.Tags.FirstOrDefault(dbT => dbT.Name == t.Name) ?? t).ToList();
            this.context.Add(post);
            this.context.SaveChanges();
            return post;
        }

        public Post Vote(int id, PostVote vote)
        {
            var post = this.context.Posts.Where(p => p.ID == id)
                                         .Include(p => p.Votes)
                                         .FirstOrDefault();

            var oldVote = this.context.PostVotes.Where(v => v.PostID == vote.PostID && v.UserID == vote.UserID)
                                                .FirstOrDefault();

            bool found = oldVote is not null;

            if (found)
            {
                this.RemoveVote(post, oldVote);
                if (!oldVote.Action ^ vote.Action)
                {
                    this.context.Posts.Update(post);
                    this.context.SaveChanges();
                    return post;
                }
            }

            return this.AddVote(vote, post);
        }

        /// <summary>
        /// This is a helper method and it doesn't save changes to the context like AddVote method
        /// </summary>
        /// <param name="post"></param>
        /// <param name="toRemove"></param>
        private void RemoveVote(Post post, PostVote toRemove)
        {
            if (toRemove.Action)
            {
                post.Likes--;
            }
            else
            {
                post.Dislikes--;
            }

            this.context.PostVotes.Remove(toRemove);
        }

        private Post AddVote(PostVote vote, Post post)
        {
            if (vote.Action)
            {
                post.Likes++;
            }
            else
            {
                post.Dislikes++;
            }

            this.context.PostVotes.Add(vote);
            this.context.Posts.Update(post);
            this.context.SaveChanges();
            return post;
        }

        public int PostsCount()
        {
            return this.context.Posts.Count();
        }

        public Dictionary<string, int> MostCommentedPostsForAPI()
        {
            var result = new Dictionary<string, int>();

            var top10 = this.context.Posts.Include(p => p.Comments).OrderByDescending(p => p.Comments.Count).Take(10);

            foreach (var item in top10)
            {
                result.Add(item.Title, item.Comments.Count);
            }

            return result;
        }

        public Dictionary<string, DateTime> MostRecentPostsForAPI()
        {
            var collection = this.context.Posts.OrderByDescending(p => p.TimeCreated).Take(10);

            var result = new Dictionary<string, DateTime>();

            foreach (var item in collection)
            {
                result.Add(item.Title, item.TimeCreated);
            }

            return result;
        }

        public IQueryable<Post> MostCommentedPostsForMVC()
        {

            return this.context.Posts.Include(p => p.Comments).OrderByDescending(p => p.Comments.Count).Take(10).Include(p => p.User);

        }

        public IQueryable<Post> MostRecentPostsForMVC()
        {
            return this.context.Posts.OrderByDescending(p => p.TimeCreated).Take(10).Include(p => p.User).Include(p => p.Comments);
        }
    }
}
