﻿using Forum_Management_System.Models;

namespace Forum_Management_System.Controllers.Helpers
{
    public interface IAuthHelper
    {
        User TryGetUser(string username, string password);
        User IsPasswordCorrect(User user, string password);
        bool IsAdmin(User currentUser);
    }
}