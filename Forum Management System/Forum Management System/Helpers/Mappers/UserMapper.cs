﻿using Forum_Management_System.Dtos.Requests;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Dtos.Responses;
using System;
using System.Text.RegularExpressions;


namespace Forum_Management_System.Models.Mappers
{
    public class UserMapper : IUserMapper
    {
        public User ConvertToUserCreate(CreateUserDto dto)
        {
            this.IsPasswordValid(dto.Password);
            this.IsEmailValid(dto.Email);

            var model = new User
            {
                Username = dto.Username,
                Password = dto.Password,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email
            };

            return model;
        }
        public User ConvertToUserUpdate(User user, UpdateUserDto dto)
        {
            this.IsPasswordValid(dto.Password);
            this.IsEmailValid(dto.Email);

            user.FirstName = dto.FirstName ?? user.FirstName;
            user.LastName = dto.LastName ?? user.LastName;
            user.Password = dto.Password ?? user.Password;
            user.Email = dto.Email ?? user.Email;

            return user;
        }
        public UserLimitedAccessInfoDto ConvertUserToLimitedInfoDto(User user)
        {
            var (_, username, firstName, lastName, _, _, role, timecreated, _) = user;
            var photoURL = (user.Photo is null) ? "default photo" : user.Photo.PhotoURL;
            var userLimitedAccessInfo = new UserLimitedAccessInfoDto(username, firstName, lastName, role, timecreated, photoURL);
            return userLimitedAccessInfo;
        }
        public UserFullInfoDto ConvertUserToFullInfoDto(User user)
        {

            var (userID, username, firstName, lastName, password, email, role, timecreated, isBlocked) = user;
            var photoURL = (user.Photo is null) ? "default photo" : user.Photo.PhotoURL;

            string phoneNumber;

            if (user.Role == UserRole.admin && user.UserAdditionalInfo is not null)
            {
                phoneNumber = user.UserAdditionalInfo.PhoneNumber;
            }
            else if (user.Role == UserRole.admin && user.UserAdditionalInfo is null)
            {
                phoneNumber = "not set";
            }
            else
            {
                phoneNumber = "not allowed";
            }

            var userFullInfo = new UserFullInfoDto(userID, username, firstName, lastName, password, email, phoneNumber, timecreated, role, isBlocked, photoURL);
            return userFullInfo;
        }
        public bool IsEmailValid(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return true;
            }

            var patternEmail = new Regex(@"[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+");

            return patternEmail.Match(email).Success ? true 
                : throw new InvalidUserInputException(String.Format(ErrorMessages.InvalidParameter, "email"));
        }
        public bool IsPasswordValid(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                return true;
            }

            //Minimum eight characters, at least one upper case English letter, one lower case English letter, one number and one special character

            var patternPassword = new Regex("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$");

            return patternPassword.Match(password).Success ? true 
                : throw new InvalidUserInputException(String.Join(Environment.NewLine, String.Format(ErrorMessages.InvalidParameter, "password"), 
                "Password must be minimum 8 characters long, at least 1 upper case English letter, 1 lower case English letter, 1 number and 1 special character."));
        }
        public bool IsPhoneNumberValid(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                return true;
            }

            Regex patternPhoneNUmber = new Regex(@"^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$");

            if (phoneNumber != "Not set." && !patternPhoneNUmber.Match(phoneNumber).Success)
            {
                throw new InvalidUserInputException(String.Format(ErrorMessages.InvalidParameter, "phone number"));
            }
            else
            {
                return true;
            }
        }
    }
}
