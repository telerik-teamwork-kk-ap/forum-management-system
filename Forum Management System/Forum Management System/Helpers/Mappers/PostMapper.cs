﻿using Forum_Management_System.Models.Contracts;
using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Services;
using Forum_Management_System.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum_Management_System.Models.Mappers
{
    public class PostMapper : IPostMapper
    {
        private readonly ICommentMapper commentMapper;

        public PostMapper(ICommentMapper commentMapper)
        {
            this.commentMapper = commentMapper;
        }

        public Post ConvertToModel(PostCreateDTO dto)
        {
            var (userID, title, body, tagStrings, imageURLs) = dto;
            var images = imageURLs.Select(e => new PostImage(e)).ToList();
            var tags = tagStrings.Select(t => new Tag(t)).ToList();
            var post = new Post(0, userID, body, title, tags, images);
            return post;
        }

        public Post UpdateModel(Post toUpdate, PostUpdateDTO dto)
        {
            var (_, title, body, tagStrings) = dto;
            toUpdate.Title = title ?? toUpdate.Title;
            toUpdate.Body = body ?? toUpdate.Body;
            var tags = tagStrings.Select(t => new Tag(t)).ToList();
            toUpdate.Tags = tags;
            return toUpdate;
        }

        public PostResponseInListDTO ConvertToPostResponseInList(Post post)
        {
            var (id, user, title, _, likes, dislikes, time, comments, images) = post;
            string username = user == null ? "[deleted]" : user.Username;
            DateTime mostRecentCommentTime = comments.Count == 0 ? DateTime.MinValue : comments.Max(c => c.TimeCreated);
            List<string> tagStrings = post.Tags.Select(t => t.ToString()).ToList();
            int commentCount = comments.Count;
            return new PostResponseInListDTO(id, username, title, tagStrings, likes, dislikes, time, mostRecentCommentTime, commentCount);
        }

        public PostResponseDTO ConvertToPostResponse(Post post)
        {
            var (id, user, title, body, likes, dislikes, time, comments, images) = post;
            string username = user == null ? "[deleted]" : user.Username;
            List<string> imageURLs = images.Select(i => i.URL).ToList();
            List<CommentResponseDTO> mappedComments = comments.Select(c => this.commentMapper.ConvertToCommentResponse(c)).ToList();
            List<string> tagStrings = post.Tags.Select(t => t.ToString()).ToList();
            return new PostResponseDTO(id, username, title, body, tagStrings, likes, dislikes, imageURLs, mappedComments, time);
        }
    }
}
