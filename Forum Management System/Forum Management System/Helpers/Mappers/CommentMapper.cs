﻿using Forum_Management_System.Models.Contracts;
using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Repositories;
using System;

namespace Forum_Management_System.Models.Mappers
{
    public class CommentMapper : ICommentMapper
    {
        public CommentMapper()
        {
        }

        public CommentResponseDTO ConvertToCommentResponse(Comment comment)
        {
            var (id, user, body, likes, dislikes, timeCreated, _, isSolution, quote) = comment;
            string username = user == null ? "[deleted]" : user.Username;
            var dto = new CommentResponseDTO(id, username, quote, body, likes, dislikes, timeCreated, isSolution);
            return dto;
        }

        public Comment ConvertToModel(CommentCreateDTO dto)
        {
            var (userID, postID, body) = dto;


            var comment = new Comment(0, userID, body, postID);

            return comment;
        }

        public Comment UpdateModel(Comment toUpdate, CommentUpdateDTO dto)
        {
            var (_, _, body) = dto;
            toUpdate.Body = body;
            return toUpdate;
        }
    }
}
