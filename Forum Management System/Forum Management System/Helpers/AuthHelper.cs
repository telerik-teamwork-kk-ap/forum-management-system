﻿using Forum_Management_System.Exceptions;
using Forum_Management_System.Models;
using Forum_Management_System.Services;

namespace Forum_Management_System.Controllers.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private readonly IUsersServices usersService;
        public AuthHelper(IUsersServices usersService)
        {
            this.usersService = usersService;
        }
        public User TryGetUser(string username, string password)
        {
            try
            {
                var user = this.usersService.GetForInternalUseOnly(username);
                return this.IsPasswordCorrect(user, password);
            }
            catch (EntityNotFoundException)
            {
                throw new NotAuthenticatedOperationException(string.Format(ErrorMessages.InvalidParameter));
            }
        }
        public User IsPasswordCorrect(User user, string password)
        {
            if (user.Password != password)
            {
                throw new NotAuthenticatedOperationException(string.Format(ErrorMessages.InvalidParameter));
            }
            return user;
        }
        public bool IsAdmin(User currentUser)
        {
            return (currentUser.Role == UserRole.admin);
        }
    }
}
