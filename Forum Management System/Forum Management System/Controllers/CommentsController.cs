﻿using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Models.ViewModels;
using Forum_Management_System.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Forum_Management_System.Controllers
{
    public class CommentsController : Controller
    {
        ICommentService commentService;

        public CommentsController(ICommentService commentService)
        {
            this.commentService = commentService;
        }

        [HttpGet("Posts/Details/{postID}/Comments/{id}")]
        public IActionResult Delete(int postID, int id)
        {
            if (!this.HttpContext.Session.Keys.Contains("UserRole"))
            {
                return this.RedirectToAction("Login", "Authentication");
            }
            if (!(this.HttpContext.Session.GetString("UserRole") == "admin"))
            {
                return this.Unauthorized();
            }
            this.commentService.Delete(postID, id);
            return this.RedirectToAction(actionName: "Index", controllerName: "Posts");
        }

        [HttpGet("Posts/Details/{postID}/Comments/{id}/Like")]
        public IActionResult Like(int postID, int id)
        {
            if (!this.IsLoggedIn())
            {
                return this.RedirectToAction("Login", "Authentication");
            }
            this.commentService.Vote(this.GetVote(id, true));
            return this.RedirectToAction(actionName: "Details", controllerName: "Posts", new { id = postID });
        }

        [HttpGet("Posts/Details/{postID}/Comments/{id}/Dislike")]
        public IActionResult Dislike(int postID, int id)
        {
            if (!this.IsLoggedIn())
            {
                return this.RedirectToAction("Login", "Authentication");
            }
            this.commentService.Vote(this.GetVote(id, false));
            return this.RedirectToAction(actionName: "Details", controllerName: "Posts", new { id = postID });
        }

        [HttpPost]
        public IActionResult Create(PostDetailsViewModel viewModel)
        {
            int userID = int.Parse(this.HttpContext.Session.GetString("UserID"));
            var commentDTO = new CommentCreateDTO(userID, viewModel.PostID, viewModel.CommentBody);
            this.commentService.Create(commentDTO);
            return this.RedirectToAction(actionName: "Details", controllerName: "Posts", new { id = viewModel.PostID });
        }
        private bool IsLoggedIn()
        {
            return this.HttpContext.Session.Keys.Contains("Username");
        }

        private bool IsAdmin()
        {
            return this.HttpContext.Session.GetString("UserRole") == "admin";
        }

        private CommentVoteDTO GetVote(int id, bool action)
        {
            int userID = int.Parse(this.HttpContext.Session.GetString("UserID"));
            var vote = new CommentVoteDTO(id, userID, action);
            return vote;
        }
    }
}
