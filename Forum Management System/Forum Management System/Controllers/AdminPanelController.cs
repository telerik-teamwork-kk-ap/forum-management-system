﻿using Forum_Management_System.Controllers.Helpers;
using Forum_Management_System.Dtos.Responses;
using Forum_Management_System.Models;
using Forum_Management_System.Models.QueryParameters;
using Forum_Management_System.Services;
using Forum_Management_System.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Forum_Management_System.Controllers
{
    public class AdminPanelController : Controller
    {
        private readonly IUsersServices usersServices;

        public AdminPanelController(IUsersServices usersServices)
        {
            this.usersServices = usersServices;
        }
        public IActionResult Index([FromQuery] UserQueryParameters parameters)
        {
            var users = this.usersServices.Get(parameters, UserRole.admin) as IEnumerable<UserFullInfoDto>;

            return this.View(model: users);
        }
    }
}
