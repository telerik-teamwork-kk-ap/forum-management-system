﻿using Forum_Management_System.Services;
using Forum_Management_System.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;

namespace Forum_Management_System.Controllers.Api
{
    [Route("api/home")]
    [ApiController]
    public class HomeAPIController : ControllerBase
    {
        private readonly IUsersServices usersServices;
        private readonly IPostService postServices;
        public HomeAPIController(IUsersServices usersServices, IPostService postServices)
        {
            this.usersServices = usersServices;
            this.postServices = postServices;
        }

        [HttpGet("")]
        public IActionResult Get()
        {
            // Response in JSON format:
            
            var sb = new StringBuilder();

            sb.AppendLine(this.usersServices.UserCount());
            sb.AppendLine(this.postServices.PostsCount());
            sb.AppendLine("Most commented posts:");
            sb.AppendLine(this.postServices.MostCommentedPostsForAPI());
            sb.AppendLine("Most recent posts:");
            sb.AppendLine(this.postServices.MostRecentPostsForAPI());

            return this.StatusCode(StatusCodes.Status200OK, new { text = sb.ToString()});

            //Response in HTML format:

           /* var html = @$"<!DOCTYPE = html>
                         <html>
                         <title> FMS Home </title>
                         <body> 
<p>{this.usersServices.UserCount()}</p>
<p>{this.postServices.PostsCount()}</p>
<p>Most commented posts: </p>
<p>{this.postServices.MostCommentedPostsForAPI()}</p>
<p>Most recent posts: </p>
<p>{this.postServices.MostRecentPostsForAPI()}</p>
                         </body>
                         </html>";

            return new ContentResult
            {
                Content = html,
                ContentType = "text/html",
                StatusCode = 200
            };*/
        }
    }
}
