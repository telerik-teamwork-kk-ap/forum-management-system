﻿using Forum_Management_System.Exceptions;
using Forum_Management_System.Models.QueryParameters;
using Forum_Management_System.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Forum_Management_System.Controllers.Helpers;
using Forum_Management_System.Dtos.Requests;
using Forum_Management_System.Services.Contracts;

namespace Forum_Management_System.Controllers.Api
{
    [Route("api/users")]
    [ApiController]
    public class UsersAPIController : ControllerBase
    {
        private readonly IUsersServices usersServices;
        private readonly IPostService postServices;
        private readonly ICommentService commentServices;
        private readonly IAuthHelper authHelper;

        public UsersAPIController(IUsersServices usersServices, IAuthHelper authHelper, IPostService postServices, ICommentService commentServices)
        {
            this.usersServices = usersServices;
            this.authHelper = authHelper;
            this.postServices = postServices;
            this.commentServices = commentServices;
        }

        [HttpGet("")]
        public IActionResult Get([FromHeader] string username, [FromHeader] string password, [FromQuery] UserQueryParameters filterParameters)
        {
            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);

                var role = currentUser.Role;

                var users = filterParameters.IsEmpty() ? this.usersServices.GetAll(role) : this.usersServices.Get(filterParameters, role);

                return this.Ok(users);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (NotAuthenticatedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpGet("{identifier}")]
        public IActionResult GetUser([FromHeader] string username, [FromHeader] string password, string identifier)
        {
            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);

                var model = this.usersServices.Get(identifier, currentUser.Role);

                return this.Ok(model);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (NotAuthenticatedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }

        }

        [HttpGet("{identifier}/posts")]
        public IActionResult GetUserPosts([FromHeader] string username, [FromHeader] string password, string identifier)
        {
            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);

                var user = this.usersServices.GetForInternalUseOnly(identifier);

                var parameters = new PostQueryParameters { FromUser = user.Username };

                return this.Ok(this.postServices.Get(parameters));

            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (NotAuthenticatedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpGet("{identifier}/comments")]
        public IActionResult GetUserComments([FromHeader] string username, [FromHeader] string password, string identifier)
        {
            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);

                var user = this.usersServices.GetForInternalUseOnly(identifier);

                var parameters = new CommentQueryParameters { FromUser = user.Username };

                return this.Ok(this.commentServices.Get(0, parameters));

            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (NotAuthenticatedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
        }

        [HttpPost("")]
        public IActionResult Create([FromHeader] string username, [FromHeader] string password, [FromBody] CreateUserDto dto)
        {
            // Registration is available only for not logged in people.

            bool IsLoggedIn = true;

            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);
            }
            catch (NotAuthenticatedOperationException)
            {
                IsLoggedIn = false;
            }

            try
            {
                return IsLoggedIn ? this.BadRequest(ErrorMessages.LoggedUser) : this.Ok(this.usersServices.CreateUser(dto));
            }
            catch (DuplicateEntityException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (InvalidUserInputException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        [HttpPut("{identifier}")]
        public IActionResult Update([FromHeader] string username, [FromHeader] string password, [FromRoute] string identifier, [FromBody] UpdateUserDto dto)
        {
            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);

                var userToUpdate = this.usersServices.GetForInternalUseOnly(identifier);

                if (currentUser.UserID == userToUpdate.UserID || this.authHelper.IsAdmin(currentUser))
                {
                    var updatedUser = this.usersServices.UpdateUser(userToUpdate, dto, currentUser.Role);

                    return this.Ok(updatedUser);
                }
                else
                {
                    return this.StatusCode(StatusCodes.Status401Unauthorized, ErrorMessages.NotAuthorized);
                }
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (NotAuthenticatedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status403Forbidden, e.Message);
            }
            catch (InvalidUserInputException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        [HttpDelete("{identifier}")]
        public IActionResult Delete([FromHeader] string username, [FromHeader] string password, [FromRoute] string identifier)
        {
            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);

                var userToDelete = this.usersServices.GetForInternalUseOnly(identifier);

                if (currentUser.UserID == userToDelete.UserID || this.authHelper.IsAdmin(currentUser))
                {
                    var deletedUser = this.usersServices.Delete(userToDelete, currentUser.Role);

                    return this.Ok(deletedUser);
                }
                else
                {
                    return this.StatusCode(StatusCodes.Status401Unauthorized, ErrorMessages.NotAuthorized);
                }

            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (NotAuthenticatedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status403Forbidden, e.Message);
            }
        }
    }
}
