﻿using Forum_Management_System.Controllers.Helpers;
using Forum_Management_System.Dtos.Requests;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Forum_Management_System.Controllers.Api
{
    [Route("api/adminpanel")]
    [ApiController]
    public class AdminPanelAPIController : ControllerBase
    {
        private readonly IUsersServices usersServices;
        private readonly IAuthHelper authHelper;

        public AdminPanelAPIController(IUsersServices usersServices, IAuthHelper authHelper)
        {
            this.usersServices = usersServices;
            this.authHelper = authHelper;
        }

        [HttpPost("admin")]
        public IActionResult Create([FromHeader] string username, [FromHeader] string password, [FromBody] CreateAdminDto Admindto)
        {
            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);

                if (!this.authHelper.IsAdmin(currentUser))
                {
                    return this.StatusCode(StatusCodes.Status403Forbidden, ErrorMessages.NotAuthorized);
                }

                var createdAdmin = this.usersServices.CreateAdmin(Admindto);

                return this.Created($"http://localhost:5000/api/Users/{Admindto.UserID}", createdAdmin);
            }
            catch (EntityNotFoundException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (NotAuthenticatedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
            catch (InvalidUserInputException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        [HttpPut("role/{identifier}")]
        public IActionResult UpdateRole([FromHeader] string username, [FromHeader] string password, [FromRoute] string identifier, [FromBody] UpdateRoleDto userRole)
        {
            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);

                if (!this.authHelper.IsAdmin(currentUser))
                {
                    return this.StatusCode(StatusCodes.Status403Forbidden, ErrorMessages.NotAuthorized);
                }

                var updatedUser = this.usersServices.UpdateUserRole(identifier, userRole);

                return this.Ok(updatedUser);

            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (NotAuthenticatedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
            catch (InvalidUserInputException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        [HttpPut("blocking/{identifier}")]
        public IActionResult UpdateBlocking([FromHeader] string username, [FromHeader] string password, [FromRoute] string identifier, [FromBody] bool isBlocked)
        {
            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);

                if (!this.authHelper.IsAdmin(currentUser))
                {
                    return this.StatusCode(StatusCodes.Status403Forbidden, ErrorMessages.NotAuthorized);
                }

                var updatedUser = this.usersServices.UpdateUserBlocking(identifier, isBlocked);

                return this.Ok(updatedUser);

            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (NotAuthenticatedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
            catch (InvalidUserInputException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        [HttpPut("additionalInfo/{identifier}")]
        public IActionResult UpdateAdminAdditionalInfo([FromHeader] string username, [FromHeader] string password, [FromRoute] string identifier, [FromBody] UpdateUserAdditionalFeaturesDto addInfo)
        {
            try
            {
                var currentUser = this.authHelper.TryGetUser(username, password);

                if (!this.authHelper.IsAdmin(currentUser))
                {
                    return this.StatusCode(StatusCodes.Status403Forbidden, ErrorMessages.NotAuthorized);
                }

                var updatedUser = this.usersServices.UpdateAdminAdditionalInfo(identifier, addInfo);

                return this.Ok(updatedUser);

            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (NotAuthenticatedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
            catch (InvalidUserInputException e)
            {
                return this.BadRequest(e.Message);
            }
        }
    }
}
