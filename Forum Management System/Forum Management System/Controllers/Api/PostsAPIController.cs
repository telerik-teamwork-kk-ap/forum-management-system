﻿using Forum_Management_System.Controllers.Helpers;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Models.QueryParameters;
using Forum_Management_System.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Forum_Management_System.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsAPIController : ControllerBase
    {
        private readonly IPostService postService;
        private readonly IAuthHelper authHelper;

        public PostsAPIController(IPostService postService, IAuthHelper authHelper)
        {
            this.postService = postService;
            this.authHelper = authHelper;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var user = this.postService.Get(id);
                return this.Ok(user);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet("")]
        public IActionResult Get([FromQuery] PostQueryParameters parameters)
        {
            try
            {
                return this.Ok(this.postService.Get(parameters));
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPost("")]
        public IActionResult Create([FromHeader] string username, [FromHeader] string password, [FromBody] PostCreateDTO dto)
        {
            try
            {
                var loggedIn = this.authHelper.TryGetUser(username, password);

                if (loggedIn.IsBlocked)
                {
                    return this.Unauthorized(ErrorMessages.BlockedUser);
                }

                dto = dto with { UserID = loggedIn.UserID };
                var post = this.postService.Create(dto);

                return this.Ok(post);
            }
            catch (UnauthorizedOperationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromHeader] string username, [FromHeader] string password, [FromBody] PostUpdateDTO dto, int id)
        {
            try
            {
                var loggedIn = this.authHelper.TryGetUser(username, password);
                var toUpdate = this.postService.Get(id);

                if (toUpdate.UserUsername == loggedIn.Username)
                {
                    dto = dto with { ID = id };
                    var result = this.postService.Update(dto);
                    return this.Ok(result);
                }

                return this.Unauthorized(ErrorMessages.NotAuthorized);
            }
            catch (UnauthorizedOperationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string username, [FromHeader] string password, int id)
        {
            try
            {
                var loggedIn = this.authHelper.TryGetUser(username, password);
                var toDelete = this.postService.Get(id);

                if (toDelete.UserUsername == loggedIn.Username || this.authHelper.IsAdmin(loggedIn))
                {
                    var result = this.postService.Delete(id);
                    return this.Ok(result);
                }

                return this.Unauthorized(ErrorMessages.NotAuthorized);
            }
            catch (UnauthorizedOperationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}/voting")]
        public IActionResult Vote([FromHeader] string username, [FromHeader] string password, int id, [FromBody] PostVoteDTO dto)
        {
            try
            {
                var loggedIn = this.authHelper.TryGetUser(username, password);

                if (loggedIn.IsBlocked)
                {
                    return this.Unauthorized(ErrorMessages.BlockedUser);
                }

                dto = dto with { UserID = loggedIn.UserID, PostID = id };
                var post = this.postService.Vote(dto);

                return this.Ok(post);
            }
            catch (UnauthorizedOperationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }
    }
}
