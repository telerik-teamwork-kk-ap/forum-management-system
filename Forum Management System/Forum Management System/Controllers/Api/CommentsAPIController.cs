﻿using Forum_Management_System.Controllers.Helpers;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Models.QueryParameters;
using Forum_Management_System.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Forum_Management_System.Controllers.Api
{
    [Route("api/posts")]
    [ApiController]
    public class CommentsAPIController : ControllerBase
    {
        private readonly ICommentService commentService;
        private readonly IAuthHelper authHelper;

        public CommentsAPIController(ICommentService commentService, IAuthHelper authHelper)
        {
            this.commentService = commentService;
            this.authHelper = authHelper;
        }

        [HttpGet("{postID}/[controller]")]
        public IActionResult Get(int postID, [FromQuery] CommentQueryParameters filterParameters)
        {
            try
            {
                var result = this.commentService.Get(postID, filterParameters);
                return this.Ok(result);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet("{postID}/[controller]/{id}")]
        public IActionResult Get(int postID, int id)
        {
            try
            {
                var result = this.commentService.Get(postID, id);
                return this.Ok(result);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch( Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPost("{postID}/[controller]")]
        public IActionResult Create([FromHeader] string username, [FromHeader] string password, int postID, [FromBody] CommentCreateDTO dto)
        {
            try
            {
                var loggedIn = this.authHelper.TryGetUser(username, password);

                if (loggedIn.IsBlocked)
                {
                    return this.Unauthorized(ErrorMessages.BlockedUser);
                }

                dto = dto with { PostID = postID, UserID = loggedIn.UserID };
                var result = this.commentService.Create(dto);

                return this.Ok(result);
            }
            catch (UnauthorizedOperationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut("{postID}/[controller]/{id}")]
        public IActionResult Update([FromHeader] string username, [FromHeader] string password, int id, int postID, [FromBody] CommentUpdateDTO dto)
        {
            try
            {
                var loggedIn = this.authHelper.TryGetUser(username, password);
                var toUpdate = this.commentService.Get(postID, id);

                if (toUpdate.UserUsername == loggedIn.Username)
                {
                    dto = dto with { ID = id, PostID = postID };
                    var result = this.commentService.Update(dto);
                    return this.Ok(result);
                }

                return this.Unauthorized(ErrorMessages.NotAuthorized);
            }
            catch (UnauthorizedOperationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }

        [HttpDelete("{postID}/[controller]/{id}")]
        public IActionResult Delete([FromHeader] string username, [FromHeader] string password, int id, int postID)
        {
            try
            {
                var loggedIn = this.authHelper.TryGetUser(username, password);
                var toDelete = this.commentService.Get(postID, id);

                if (toDelete.UserUsername == loggedIn.Username || this.authHelper.IsAdmin(loggedIn))
                {
                    var result = this.commentService.Delete(postID, id);
                    return this.Ok(result);
                }

                return this.Unauthorized(ErrorMessages.NotAuthorized);
            }
            catch (UnauthorizedOperationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }

        [HttpPut("{postID}/[controller]/{id}/voting")]
        public IActionResult Vote([FromHeader] string username, [FromHeader] string password, int id, [FromBody] CommentVoteDTO dto)
        {
            try
            {
                var loggedIn = this.authHelper.TryGetUser(username, password);
                dto = dto with { CommentID = id, UsedID = loggedIn.UserID };
                var result = this.commentService.Vote(dto);

                return this.Ok(result);
            }
            catch (UnauthorizedOperationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(ex.Message))
                {
                    return this.BadRequest(ex);
                }
                return this.BadRequest(ex.Message);
            }
        }
    }
}
