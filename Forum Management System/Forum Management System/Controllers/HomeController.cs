﻿using Forum_Management_System.Models;
using Forum_Management_System.Services;
using Forum_Management_System.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Forum_Management_System.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUsersServices usersServices;
        private readonly IPostService postServices;
        public HomeController(IUsersServices usersServices, IPostService postServices)
        {
            this.usersServices = usersServices;
            this.postServices = postServices;
        }
        public IActionResult Index()
        {
            this.ViewData["userCount"] = this.usersServices.UserCount();
            this.ViewData["postCount"] = this.postServices.PostsCount();

            var item1 = this.postServices.MostCommentedPostsForMVC();
            var item2 = this.postServices.MostRecentPostsForMVC();

            var collection = new Tuple<IEnumerable<Post>, IEnumerable<Post>>( item1, item2 );
            return this.View(collection);
        }

        public IActionResult About()
        {
            return this.View();
        }
    }
}
