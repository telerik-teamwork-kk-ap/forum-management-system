﻿using Forum_Management_System.Controllers.Helpers;
using Forum_Management_System.Dtos.Requests;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum_Management_System.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly IAuthHelper authHelper;
        private readonly IUsersServices usersServices;
        public AuthenticationController(IAuthHelper authHelper, IUsersServices usersServices)
        {
            this.authHelper = authHelper;
            this.usersServices = usersServices;
        }
        public IActionResult Login()
        {
            if (this.HttpContext.Session.Keys.Any(key => key == "Username"))
            {
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }

            return this.View();
        }

        [HttpPost]
        public IActionResult Login(LoginUserDto userCredentials)
        {
            try
            {
                var user = this.authHelper.TryGetUser(userCredentials.Username, userCredentials.Password);
                this.HttpContext.Session.SetString("Username", user.Username);
                this.HttpContext.Session.SetString("UserID", user.UserID.ToString());
                this.HttpContext.Session.SetString("UserRole", user.Role.ToString());
            }
            catch (NotAuthenticatedOperationException e)
            {
                this.ViewData["Failed"] = e.Message;
                return this.View();
            }

            return this.RedirectToAction(actionName:"Index", controllerName:"Posts");
        }
        public IActionResult Logout()
        {
            this.HttpContext.Session.Clear();
            
            return this.RedirectToAction(actionName: "Index", controllerName: "Home");
        }

        public IActionResult Register()
        {
            if (this.HttpContext.Session.Keys.Any(key => key == "Username"))
            {
                return this.RedirectToAction(actionName: "Index", controllerName: "Posts");
            }

            return this.View();
        }

        [HttpPost]
        public IActionResult Register(CreateUserDto dto)
        {

            try
            {
                var user = this.usersServices.CreateUser(dto);
                this.HttpContext.Session.SetString("Username", user.Username);
                this.HttpContext.Session.SetString("UserRole", user.Role.ToString());
            }
            catch (Exception e)
            {
                this.ViewData["Failed"] = e.Message;
                return this.View();
            }
            return this.RedirectToAction(actionName: "Index", controllerName: "Posts");
        }
    }
}
