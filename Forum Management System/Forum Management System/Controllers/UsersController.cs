﻿using Forum_Management_System.Controllers.Helpers;
using Forum_Management_System.Dtos.Requests;
using Forum_Management_System.Dtos.Responses;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Models;
using Forum_Management_System.Models.QueryParameters;
using Forum_Management_System.Services;
using Forum_Management_System.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Forum_Management_System.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUsersServices usersServices;

        public UsersController(IUsersServices usersServices)
        {
            this.usersServices = usersServices;
        }

        public IActionResult Edit()
        {
            var username = this.HttpContext.Session.GetString("Username");
            var user = this.usersServices.GetForInternalUseOnly(username);
            var photoURL = (user.Photo is null) ? "https://localhost:5000/images/default_photo.jpg" : user.Photo.PhotoURL;
            //var phoneNumber = (user.UserAdditionalInfo is null) ? "0000000000" : user.UserAdditionalInfo.PhoneNumber;
            var infoForUpdate = new UpdateUserDto(user.FirstName, user.LastName, user.Password, user.Email, photoURL);
            return this.View(model: infoForUpdate);
        }

        [HttpPost]
        public IActionResult Edit(UpdateUserDto userInfo)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model: userInfo);
            }

            var username = this.HttpContext.Session.GetString("Username");
            var user = this.usersServices.GetForInternalUseOnly(username);
            var updatedUser = this.usersServices.UpdateUser(user, userInfo, user.Role);
            return this.RedirectToAction(actionName: "Details", controllerName: "Users");
        }

        [HttpPost]
        public IActionResult Delete(string accountActivation)
        {
            if (accountActivation == "on")
            {
                var username = this.HttpContext.Session.GetString("Username");
                var user = this.usersServices.GetForInternalUseOnly(username);
                this.usersServices.Delete(user, user.Role);
                this.HttpContext.Session.Clear();
                return this.RedirectToAction(controllerName: "Home", actionName: "Index");
            }
            else
            {
                return this.RedirectToAction(controllerName: "Users", actionName: "Edit");
            }
        }

        public IActionResult Index([FromQuery] UserQueryParameters parameters)
        {
            var userRole = this.HttpContext.Session.GetString("UserRole");
            Enum.TryParse(userRole, out UserRole role);
            var users = this.usersServices.Get(parameters, role);
            
            return this.View(model: users);
        }

        public IActionResult Details()
        {
                var username = this.HttpContext.Session.GetString("Username");
                var userRole = this.HttpContext.Session.GetString("UserRole");
                Enum.TryParse(userRole, out UserRole role);
                var user = this.usersServices.Get(username, role);
                return this.View(model: user);
        }

        [HttpGet("users/detailslink/{username}")]
        public IActionResult DetailsLink(string username)
        {
            try
            {
                var userRole = this.HttpContext.Session.GetString("UserRole");
                Enum.TryParse(userRole, out UserRole role);
                var user = this.usersServices.Get(username, role);
                return this.View(viewName: "Details", model: user);
            }
            catch (EntityNotFoundException)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                this.ViewData["ErrorMessage"] = "User does not exist.";
                return this.View("Error");
            }
        }
    }
}
