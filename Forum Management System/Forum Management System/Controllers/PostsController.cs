﻿using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Models.QueryParameters;
using Forum_Management_System.Models.ViewModels;
using Forum_Management_System.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum_Management_System.Controllers
{
    public class PostsController : Controller
    {
        private readonly IPostService postService;

        public PostsController(IPostService postService)
        {
            this.postService = postService;
        }

        public IActionResult Index([FromQuery] PostQueryParameters queryParameters)
        {
            var result = this.postService.Get(queryParameters);
            return this.View(result);
        }

        public IActionResult Details(int id)
        {
            if (!this.IsLoggedIn())
            {
                return this.RedirectToAction("Login", "Authentication");
            }
            var postDTO = this.postService.Get(id);
            var result = new PostDetailsViewModel(postDTO, "");
            return this.View(result);
        }

        public IActionResult CreateForm()
        {
            if (!this.ModelState.IsValid)
            {
                return this.View();
            }

            if (!this.IsLoggedIn())
            {
                return this.RedirectToAction("Login", "Authentication");
            }
            return this.View();
        }

        public IActionResult Create(PostCreateDTO dto)
        {
            if (!this.IsLoggedIn())
            {
                return this.RedirectToAction("Login", "Authentication");
            }
            var tags = this.ExtractTags(dto.Body);
            dto = dto with { Tags = tags, ImageURLs = new List<string>(), UserID = this.GetUserID() };
            this.postService.Create(dto);
            return this.RedirectToAction("Index", "Posts");
        }

        public IActionResult Delete(int id)
        {
            if (!this.IsLoggedIn())
            {
                return this.RedirectToAction("Login", "Authentication");
            }
            if (!this.IsAdmin())
            {
                return this.Unauthorized();
            }
            this.postService.Delete(id);
            return this.RedirectToAction(actionName: "Index", controllerName: "Posts");
        }

        public IActionResult Like(int id)
        {
            if (!this.IsLoggedIn())
            {
                return this.RedirectToAction("Login", "Authentication");
            }
            this.postService.Vote(this.GetVote(id, true));
            return this.RedirectToAction("Details", "Posts", new { id });
        }

        public IActionResult Dislike(int id)
        {
            if (!this.IsLoggedIn())
            {
                return this.RedirectToAction("Login", "Authentication");
            }
            this.postService.Vote(this.GetVote(id, false));
            return this.RedirectToAction("Details", "Posts", new { id });
        }

        private bool IsLoggedIn()
        {
            return this.HttpContext.Session.Keys.Contains("Username");
        }

        private int GetUserID()
        {
            return int.Parse(this.HttpContext.Session.GetString("UserID"));
        }

        private bool IsAdmin()
        {
            return this.HttpContext.Session.GetString("UserRole") == "admin";
        }

        private PostVoteDTO GetVote(int id, bool action)
        {
            int userID = int.Parse(this.HttpContext.Session.GetString("UserID"));
            var vote = new PostVoteDTO(id, userID, action);
            return vote;
        }

        public IEnumerable<string> ExtractTags(string body)
        {
            if (!string.IsNullOrEmpty(body))
            {
                var words = body.Split(' ', '\n');
                foreach (var word in words)
                {
                    if (word.Length > 1 && word[0] == '#')
                    {
                        yield return word[1..];
                    }
                }
            }
        }
    }
}
