﻿using Forum_Management_System.Models;
using Forum_Management_System.Models.Contracts;
using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Models.Mappers;
using Forum_Management_System.Models.QueryParameters;
using Forum_Management_System.Repositories;
using Forum_Management_System.Services.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace Forum_Management_System.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentsRepository commentRepository;
        private readonly ICommentMapper commentMapper;

        public CommentService(ICommentsRepository commentRepository, ICommentMapper commentMapper)
        {
            this.commentRepository = commentRepository;
            this.commentMapper = commentMapper;
        }

        public CommentResponseDTO Create(CommentCreateDTO dto)
        {
            var comment = this.commentMapper.ConvertToModel(dto);
            var repoResponse = this.commentRepository.Create(comment);
            var result = this.commentMapper.ConvertToCommentResponse(repoResponse);
            return result;
        }

        public CommentResponseDTO Delete(int postID, int id)
        {
            var repoResponse = this.commentRepository.Delete(postID, id);
            var result = this.commentMapper.ConvertToCommentResponse(repoResponse);
            return result;
        }

        public CommentResponseDTO Get(int postID, int id)
        {
            var repoResponse = this.commentRepository.Get(postID, id);
            var result = this.commentMapper.ConvertToCommentResponse(repoResponse);
            return result;
        }

        public IEnumerable<CommentResponseDTO> Get(int postID = 0, CommentQueryParameters filterParameters = null)
        {
            var repoResponse = this.commentRepository.Get(postID, filterParameters);
            var result = repoResponse.Select(c => new CommentResponseDTO(c.ID,
                                                                         c.User == null ? "[deleted]" : c.User.Username,
                                                                         c.Quote,
                                                                         c.Body,
                                                                         c.Likes,
                                                                         c.Dislikes,
                                                                         c.TimeCreated,
                                                                         c.IsSolution));
            return result;
        }

        public CommentResponseDTO Update(CommentUpdateDTO dto)
        {
            var (postID, id, _) = dto;
            var toUpdate = this.commentRepository.Get(postID, id);
            var updated = this.commentMapper.UpdateModel(toUpdate, dto);
            var repoResponse = this.commentRepository.Update(updated);
            var result = this.commentMapper.ConvertToCommentResponse(repoResponse);
            return result;
        }

        public CommentResponseDTO Vote(CommentVoteDTO dto)
        {
            var (commentID, userID, action) = dto;
            var vote = new CommentVote(commentID, userID, action);
            var repoResponse = this.commentRepository.Vote(commentID, vote);
            var result = this.commentMapper.ConvertToCommentResponse(repoResponse);
            return result;
        }
    }
}
