﻿using Forum_Management_System.Dtos.Requests;
using Forum_Management_System.Models;
using Forum_Management_System.Models.QueryParameters;
using Forum_Management_System.Dtos.Responses;
using System.Collections.Generic;

namespace Forum_Management_System.Services
{
    public interface IUsersServices
    {
        User GetForInternalUseOnly(string username);
        IGetUserDto Get(string identifier, UserRole role);
        IEnumerable<IGetUserDto> GetAll(UserRole role);
        IEnumerable<IGetUserDto> Get(UserQueryParameters filterParameters, UserRole role);
        IGetUserDto CreateUser(CreateUserDto userToBeCreated);
        UserFullInfoDto CreateAdmin(CreateAdminDto adminDto);
        IGetUserDto UpdateUser(User userToUpdate, UpdateUserDto infoToUpdate, UserRole currentRole);
        UserFullInfoDto UpdateUserRole(string identifier, UpdateRoleDto userRole);
        UserFullInfoDto UpdateUserBlocking(string identifier, bool isBlocked);    
        UserFullInfoDto UpdateAdminAdditionalInfo(string identifier, UpdateUserAdditionalFeaturesDto addInfo);
        IGetUserDto Delete(User userToDelete, UserRole role);
        IGetUserDto CheckRole(UserRole role, User user);
        string UserCount();
    }
}

