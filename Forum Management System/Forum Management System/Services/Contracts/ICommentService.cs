﻿using Forum_Management_System.Models;
using Forum_Management_System.Models.Contracts;
using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Models.QueryParameters;
using System.Collections.Generic;

namespace Forum_Management_System.Services.Contracts
{
    public interface ICommentService
    {
        CommentResponseDTO Get(int PostID, int id);

        IEnumerable<CommentResponseDTO> Get(int postID, CommentQueryParameters filterParameters = null);

        CommentResponseDTO Create(CommentCreateDTO dto);

        CommentResponseDTO Update(CommentUpdateDTO dto);

        CommentResponseDTO Delete(int postID, int id);

        CommentResponseDTO Vote(CommentVoteDTO dto);
    }
}