﻿using Forum_Management_System.Models;
using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Models.QueryParameters;
using System;
using System.Collections.Generic;

namespace Forum_Management_System.Services.Contracts
{
    public interface IPostService
    {
        PostResponseDTO Get(int id);
        IEnumerable<PostResponseInListDTO> Get(PostQueryParameters filterParameters = null);
        PostResponseDTO Create(PostCreateDTO dto);
        PostResponseDTO Update(PostUpdateDTO dto);
        PostResponseDTO Delete(int id);

        PostResponseDTO Vote(PostVoteDTO dto);
        string PostsCount();
        string MostCommentedPostsForAPI();
        string MostRecentPostsForAPI();
        IEnumerable<Post> MostCommentedPostsForMVC();
        IEnumerable<Post> MostRecentPostsForMVC();
    }
}
