﻿using Forum_Management_System.Constants;
using Forum_Management_System.Models;
using Forum_Management_System.Models.Contracts;
using Forum_Management_System.Models.Dtos;
using Forum_Management_System.Models.QueryParameters;
using Forum_Management_System.Repositories;
using Forum_Management_System.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forum_Management_System.Services
{
    public class PostService : IPostService
    {
        private readonly IPostsRepository postsRepository;
        private readonly IPostMapper postsMapper;

        public PostService(IPostsRepository postsRepository, IPostMapper postMapper)
        {
            this.postsRepository = postsRepository;
            this.postsMapper = postMapper;
        }

        public PostResponseDTO Create(PostCreateDTO post)
        {
            Post mappedPost = this.postsMapper.ConvertToModel(post);
            var repoResponse = this.postsRepository.Create(mappedPost);
            var result = this.postsMapper.ConvertToPostResponse(repoResponse);
            return result;
        }

        public PostResponseDTO Delete(int id)
        {
            var repoResponse = this.postsRepository.Delete(id);
            var result = this.postsMapper.ConvertToPostResponse(repoResponse);
            return result;
        }

        public PostResponseDTO Get(int id)
        {
            var repoResponse = this.postsRepository.Get(id);
            var result = this.postsMapper.ConvertToPostResponse(repoResponse);
            return result;
        }

        public IEnumerable<PostResponseInListDTO> Get(PostQueryParameters filterParameters = null)
        {
            var filteredPosts = this.postsRepository.Get(filterParameters);
            var result = filteredPosts.Select(p => new PostResponseInListDTO(p.ID,
                                                                             p.User == null ? "[deleted]" : p.User.Username,
                                                                             p.Title,
                                                                             p.Tags.Select(t => t.Name).ToList(),
                                                                             p.Likes,
                                                                             p.Dislikes,
                                                                             p.TimeCreated,
                                                                             p.Comments.Count == 0 ? DateTime.MinValue : p.Comments.Max(c => c.TimeCreated),
                                                                             p.Comments.Count)).ToList();
            return result;
        }

        public PostResponseDTO Update(PostUpdateDTO dto)
        {
            int id = dto.ID;
            var toUpdate = this.postsRepository.Get(id);
            var updated = this.postsMapper.UpdateModel(toUpdate, dto);
            var repoResponse = this.postsRepository.Update(updated);
            var result = this.postsMapper.ConvertToPostResponse(repoResponse);
            return result;
        }

        public string PostsCount()
        {
            return string.Format(HomePageMessages.PostCount, this.postsRepository.PostsCount());
        }

        public string MostCommentedPostsForAPI()
        {
            var collection = this.postsRepository.MostCommentedPostsForAPI();

            var result = new StringBuilder();

            foreach (var item in collection)
            {
                result.Append(item.Key + " - ");
                result.AppendLine(item.Value + " comments");
            }

            return result.ToString().TrimEnd();
        }

        public string MostRecentPostsForAPI()
        {
            var collection = this.postsRepository.MostRecentPostsForAPI();

            var result = new StringBuilder();

            foreach (var item in collection)
            {
                result.Append(item.Key + " ");
                result.AppendLine(item.Value.ToShortDateString());
            }

            return result.ToString().TrimEnd();
        }

        public IEnumerable<Post> MostCommentedPostsForMVC()
        {
            return this.postsRepository.MostCommentedPostsForMVC();
        }

        public IEnumerable<Post> MostRecentPostsForMVC()
        {
            return this.postsRepository.MostRecentPostsForMVC();
        }

        public PostResponseDTO Vote(PostVoteDTO dto)
        {
            var (postID, userID, action) = dto;
            var vote = new PostVote(postID, userID, action);
            var repoResponse = this.postsRepository.Vote(postID, vote);
            var result = this.postsMapper.ConvertToPostResponse(repoResponse);
            return result;
        }
    }
}
