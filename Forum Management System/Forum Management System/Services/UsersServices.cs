﻿using Forum_Management_System.Constants;
using Forum_Management_System.Dtos.Requests;
using Forum_Management_System.Exceptions;
using Forum_Management_System.Models;
using Forum_Management_System.Models.QueryParameters;
using Forum_Management_System.Repositories;
using Forum_Management_System.Dtos.Responses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum_Management_System.Services
{
    public class UsersServices : IUsersServices
    {
        private readonly IUsersRepository repository;
        private readonly IPhotosRepository photosRepository;
        private readonly IUserMapper userMapper;

        public UsersServices(IUsersRepository repository, IUserMapper mapper, IPhotosRepository photosRepository)
        {
            this.repository = repository;
            this.userMapper = mapper;
            this.photosRepository = photosRepository;
        }
        public User GetForInternalUseOnly(string identifier)
        {
            return int.TryParse(identifier, out int id) ? this.repository.Get(id) : this.repository.Get(identifier);
        }
        public IGetUserDto Get(string identifier, UserRole role)
        {
            User user = this.GetForInternalUseOnly(identifier);

            return this.CheckRole(role, user);
        }
        public IEnumerable<IGetUserDto> GetAll(UserRole role)
        {
            var collection = this.repository.Get();

            if (!collection.Any())
            {
                throw new EntityNotFoundException(ErrorMessages.EmptyCollection);
            }

            switch (role)
            {
                case UserRole.admin:
                    return collection.Select(user => this.userMapper.ConvertUserToFullInfoDto(user)).ToList();

                default:
                    return collection.Select(user => this.userMapper.ConvertUserToLimitedInfoDto(user)).ToList();
            }
        }

        public IEnumerable<IGetUserDto> Get(UserQueryParameters filterParameters, UserRole role)
        {
            var collection = this.repository.Get(filterParameters);

            switch (role)
            {
                case UserRole.admin:
                    return collection.Select(user => this.userMapper.ConvertUserToFullInfoDto(user)).ToList();

                default:
                    return collection.Select(user => this.userMapper.ConvertUserToLimitedInfoDto(user)).ToList();
            }
        }
        public IGetUserDto CreateUser(CreateUserDto userToBeCreated)
        {
            // Checks if such user already exists in the database.
            bool userExists = true;

            try
            {
                this.repository.Get(userToBeCreated.Username);
            }
            catch (EntityNotFoundException)
            {
                userExists = false;
            }

            if (userExists)
            {
                throw new DuplicateEntityException(String.Format(ErrorMessages.DuplicateItem, "user", "username", userToBeCreated.Username));
            }

            // Converts from RequestDTO to User object. Checks if email and password are valid.

            User userModel = this.userMapper.ConvertToUserCreate(userToBeCreated);

            // Checks whether the email is unique.

            if (!this.repository.IsEmailUnique(userModel.Email))
            {
                throw new DuplicateEntityException(String.Format(ErrorMessages.DuplicateItem, "user", "email", userToBeCreated.Email));
            }

            // Creates a new user.

            var newUser = this.repository.CreateUser(userModel);
            var (_, username, firstName, lastName, _, _, role, timeCreated, _) = newUser;

            // Adds a photo if requested.

            string photoUrl = "default photo";

            if (!string.IsNullOrEmpty(userToBeCreated.PhotoURL))
            {
                photoUrl = this.photosRepository.Create(newUser, userToBeCreated.PhotoURL).PhotoURL;
            }

            // Converts User to ResponseDTO

            return new UserLimitedAccessInfoDto(username, firstName, lastName, role, timeCreated, photoUrl);
        }
        public UserFullInfoDto CreateAdmin(CreateAdminDto adminDto)
        {
            var adminToBe = this.repository.Get(adminDto.UserID);
            if (adminToBe.Role == UserRole.admin)
            {
                throw new InvalidUserInputException(String.Format(ErrorMessages.SameRole, adminDto.UserID, adminToBe.Role));
            }

            string phoneNumber = null;

            if (adminDto.PhoneNumber is not null)
            {
                phoneNumber = adminDto.PhoneNumber;
                this.userMapper.IsPhoneNumberValid(phoneNumber);
            }

            var newAdmin = this.repository.CreateAdmin(adminToBe, phoneNumber);
            var createdAdmin = this.userMapper.ConvertUserToFullInfoDto(newAdmin);
            return createdAdmin;
        }
        public IGetUserDto UpdateUser(User userToUpdate, UpdateUserDto infoToUpdate, UserRole currentRole)
        {
            var userModel = this.userMapper.ConvertToUserUpdate(userToUpdate, infoToUpdate);

            var updatedUser = this.repository.Update(userModel);

            // Changes photo.

            if (!string.IsNullOrEmpty(infoToUpdate.PhotoURL))
            {
                this.photosRepository.Update(userToUpdate, infoToUpdate.PhotoURL);
            }

            return this.CheckRole(currentRole, updatedUser);
        }
        public UserFullInfoDto UpdateUserRole(string identifier, UpdateRoleDto userRole)
        {
            var userToUpdate = this.GetForInternalUseOnly(identifier);

            if (userToUpdate.Role == UserRole.admin && userRole.Role == 0)
            {
                userToUpdate.Role = (UserRole)userRole.Role;

                if (userToUpdate.UserAdditionalInfo is not null)
                {
                    this.repository.DeleteAdminAdditionalInfo(userToUpdate.UserAdditionalInfo);
                }

            }
            else if ((int)userToUpdate.Role == userRole.Role)
            {
                throw new InvalidUserInputException(String.Format(ErrorMessages.SameRole, userToUpdate.UserID, userToUpdate.Role));
            }
            else
            {
                throw new InvalidUserInputException("To create an admin, please use a post request.");
            }

            var updatedUser = this.repository.Update(userToUpdate);

            return this.userMapper.ConvertUserToFullInfoDto(updatedUser);

        }
        public UserFullInfoDto UpdateUserBlocking(string identifier, bool isBlocked)
        {
            var userToUpdate = this.GetForInternalUseOnly(identifier);

            if (userToUpdate.IsBlocked != isBlocked)
            {
                userToUpdate.IsBlocked = isBlocked;
                userToUpdate = this.repository.Update(userToUpdate);
            }

            return this.userMapper.ConvertUserToFullInfoDto(userToUpdate);
        }
        public UserFullInfoDto UpdateAdminAdditionalInfo(string identifier, UpdateUserAdditionalFeaturesDto addInfo)
        {
            var userToUpdate = this.GetForInternalUseOnly(identifier);

            if (userToUpdate.Role != UserRole.admin)
            {
                throw new InvalidUserInputException($"User with id {userToUpdate.UserID} is not an admin and is not allowed to record a phone number.");
            }

            this.userMapper.IsPhoneNumberValid(addInfo.PhoneNumber);

            User updatedUser;

            if (userToUpdate.UserAdditionalInfo is null)
            {
                updatedUser = this.repository.CreateAdmin(userToUpdate, addInfo.PhoneNumber);
            }
            else
            {
                userToUpdate.UserAdditionalInfo.PhoneNumber = addInfo.PhoneNumber;
                updatedUser = this.repository.UpdateAdminAdditionalInfo(userToUpdate.UserAdditionalInfo);
            }

            return this.userMapper.ConvertUserToFullInfoDto(updatedUser);
        }
        public IGetUserDto Delete(User userToDelete, UserRole role)
        {
            // Ensure to delete user's profile pic and additional info before deleting the user.

            if (userToDelete.Photo is not null)
            {
                this.photosRepository.Delete(userToDelete.UserID);
            }

            if (userToDelete.UserAdditionalInfo is not null)
            {
                this.repository.DeleteAdminAdditionalInfo(userToDelete.UserAdditionalInfo);
            }

            // Delete user.

            var deletedUser = this.repository.Delete(userToDelete);

            return this.CheckRole(role, deletedUser);
        }
        public IGetUserDto CheckRole(UserRole role, User user)
        {
            switch (role)
            {
                case UserRole.admin:
                    return this.userMapper.ConvertUserToFullInfoDto(user);
                default:
                    return this.userMapper.ConvertUserToLimitedInfoDto(user);
            }
        }
        public string UserCount()
        {
            return String.Format(HomePageMessages.UserCount, this.repository.UserCount());
        }

    }
}
