﻿using System;

namespace Forum_Management_System.Exceptions
{
    public class DuplicateEntityException : ApplicationException
    {
        public DuplicateEntityException(string message)
           : base(message)
        {
        }
    }
}
